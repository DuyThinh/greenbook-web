import { unwrapResult } from "@reduxjs/toolkit";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Button from "~/components/button";
import { cancelOrder, getOrders } from "~/redux/reducers/orders";
import { currentUserSelector, ordersSelector } from "~/redux/selectors";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  FaAt,
  FaEdit,
  FaMapMarkerAlt,
  FaPhone,
  FaUserCircle,
} from "react-icons/fa";
import Tippy from "@tippyjs/react";
import ReactTimeAgo from "react-time-ago";
import { parseISO } from "date-fns";
import greenBookAPI from "~/api/greenBookAPI";

export default function Payment(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const orders = useSelector(ordersSelector);
  const [showCancelOrder, setShowCancelOrder] = useState(false);
  const [color, setColor] = useState("#048957");
  const param = useParams();
  const [openCollapse, setOpenCollapse] = useState(true);
  const [index, setIndex] = useState(null);
  const [paymentId, setPaymentId] = useState("");
  const [order, setOrder] = useState([]);
  const [orderBook, setOrderBook] = useState([]);
  const currentUser = useSelector(currentUserSelector);

  const [firstName, setFirstName] = useState(currentUser.firstName || "");
  const [lastName, setLastName] = useState(currentUser.lastName || "");
  const [shipAddress, setShipAddress] = useState(currentUser.shipAddress);
  const [phone, setPhone] = useState(currentUser.mobile || "");

  const [paymentMethod, setPaymentMethod] = useState(0);
  const [isPaymentSuccess, setIsPaymentSuccess] = useState(false);

  // For alert
  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const handlePayment = async () => {
    if (paymentMethod === 0) {
      let res;
      res = await greenBookAPI.getPaymentUrl(
        order[0].order.id,
        order[0].order.total_price
      );
      setPaymentId(res.data.data.payment_id);
      window.open(res.data.data.vnpay_payment_url, "_blank");
      setTimeout(() => navigate("/orders"), 3000);
    } else {
      notify("Đơn hàng của bạn đã được xác nhận");
      setTimeout(() => navigate("/orders"), 3000);
    }
  };
  const handleCheckPayment = async () => {
    if (paymentMethod === 0) {
      const endIndex = paymentId.indexOf("_");
      let id;
      if (endIndex !== -1) {
        setPaymentId(paymentId.substring(0, endIndex));
        id = paymentId.substring(0, endIndex);
      } else {
        setPaymentId(paymentId);
        id = paymentId;
      }
      let res;
      res = await greenBookAPI.getPaymentStatus(id);
      console.log("getPaymentStatus", res.data.data);
      if (
        res.data.message === "Successfully" &&
        parseInt(res.data.code) === 200
      ) {
        setIsPaymentSuccess(true);
      } else {
        setColor("#980303");
        notify("Thanh toán chưa thành công, vui lòng thử lại!");
      }
    }
  };

  const handleShowCancelOrder = () => {
    setShowCancelOrder(!showCancelOrder);
  };

  const handleCancelOrder = () => {
    if (orders.length > 0) {
      dispatch(cancelOrder({ id: param.id }))
        .then(unwrapResult)
        .then(() => {
          setColor("#048957");
          notify("Đã huỷ đơn hàng");
        })
        .then(navigate("/cart"))
        .catch((err) => {
          setColor("#980303");
          notify("Đã xảy ra lỗi, vui lòng thử lại!");
          console.log(err.message);
        });
    }
  };

  useEffect(() => {
    if (!orders.length > 0) {
      dispatch(getOrders({}))
        .then(unwrapResult)

        .catch((err) => {
          setColor("#980303");
          notify("Đã xảy ra lỗi, vui lòng thử lại!");
          console.log(err.message);
        });
    }
    param?.id &&
      setOrder(orders.filter((item) => item.order.id.includes(param.id)));
  }, [orders]);

  useEffect(() => {
    // Get Detail book
    let getBook = [];
    if (order.length > 0) {
      order.map((order) => {
        return (getBook = [
          ...getBook,
          ...order.orderItems.map((e) => e.book.id),
        ]);
      });
    }
    const requests = getBook.map((bookId) =>
      greenBookAPI.getBook(bookId, "", currentUser.id_User)
    );
    // send all request
    Promise.all(requests)
      .then((responses) => {
        let getBookRes = [];
        // For each response
        responses.forEach((response) => {
          if (response.status === 200) {
            getBookRes = [...getBookRes, response.data.data];
            const products = getBookRes.map((item, index) => {
              return {
                id: item.id,
                price: item.price,
                name: item.name,
                weight: parseInt(item.weight),
                img: item.images[0].url,
              };
            });
            setOrderBook(products);
          } else {
            setColor("#980303");
            notify("Đã xảy ra lỗi, vui lòng try again!");
            console.log("error!");
          }
        });
      })
      .catch((error) => {
        setColor("#980303");
        notify("Lỗi khi gửi yêu cầu");
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [order]);

  return (
    <>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />

      {!param.id && (
        <div className="md:w-4/5 w-full mx-auto shadow-[#9a6f55a4] relative md:p-3 p-1 shadow-lg rounded-xl">
          <p className="flex justify-end items-center px-5">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className={`${
                openCollapse ? "rotate-180" : ""
              } h-5 w-5 transition-transform`}
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
              onClick={() => setOpenCollapse(!openCollapse)}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M19 9l-7 7-7-7"
              />
            </svg>
          </p>
          <div
            className={`flex-auto overflow-auto max-h-80 ${
              openCollapse === false ? "h-20" : ""
            }`}
          >
            <table className="w-full">
              <thead>
                <tr className="rounded-t-sm">
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                </tr>
              </thead>
              <tbody>
                {orders &&
                  orders
                    .slice(
                      !openCollapse && index !== null ? index : 0,
                      !openCollapse && index !== null
                        ? index + 1
                        : openCollapse
                        ? orders.length
                        : 1
                    )
                    .map((item, index) => (
                      <tr
                        className="gap-5 before:border-0 after:border-0"
                        key={item.order.id}
                      >
                        <td className="py-2 w-1/12">{item.order.address}</td>
                        <td className="w-4/12">
                          <p className="text-xs md:text-base overview-one">
                            {item.orderItems.length} sản phẩm
                          </p>
                        </td>
                        <td className="w-2/12">
                          <p className="text-[#1f396a] overview-one text-xs md:text-base">
                            {item.order.total_price.toLocaleString()}đ
                          </p>
                        </td>
                        <td className="w-3/12 text-xs md:text-base">
                          <ReactTimeAgo
                            date={parseISO(item.orderItems[0].created_at)}
                            locale="vi-VN"
                          />
                        </td>
                        <td className="w-1/12">
                          <Tippy placement="bottom" content={item.order.status}>
                            <div
                              className={`${
                                item.order.status === "Mới"
                                  ? "bg-[#c63623]"
                                  : "bg-[#23c67a]"
                              } w-fit px-3 py-1 text-xs rounded-lg md:text-base font-bold text-white text-center`}
                            >
                              {item.order.status}
                            </div>
                          </Tippy>
                        </td>
                        <td className="w-1/12">
                          <Tippy placement="bottom" content="Xem">
                            <div
                              onClick={() => handlePayment(index)}
                              className="text-center text-xs md:text-base cursor-pointer"
                            >
                              <span className="text-[#0b1613]">Xem</span>
                            </div>
                          </Tippy>
                        </td>
                      </tr>
                    ))}
              </tbody>
            </table>
          </div>
        </div>
      )}
      <div className="max-w-8xl mx-auto flex lg:flex-row flex-col p-1 md:p-5 rounded-lg gap-5">
        <div className="relative lg:w-3/6 w-full p-1 lg:p-5 min-h-[30vh]  bg-[#F6FFFF] rounded-2xl shadow-custom shadow-[#c5c3a3]">
          <table className="w-full">
            <thead>
              <tr className="rounded-t-sm">
                <th className="px-3 py-2"></th>
                <th className="px-3 py-2"></th>
                <th className="px-3 py-2"></th>
                <th className="px-3 py-2"></th>
              </tr>
            </thead>
            <tbody>
              {orders &&
                orders
                  .filter((item) => item.order.id.includes(param.id))
                  .map((item) =>
                    item.orderItems.map((e, index) => (
                      <tr
                        className={`${
                          item.index % 2 ? "bg-[#abf7df43]" : ""
                        } border-b-[1.5px] border-[#05a6dc69] gap-5 before:border-0 after:border-0`}
                        key={e.book.id}
                      >
                        <td className="p-2 flex justify-center items-center max-w-2/12 ">
                          <img
                            width={70}
                            src={orderBook[index]?.img ?? ""}
                            alt={orderBook[index]?.name ?? ""}
                          />
                        </td>
                        <td className="p-2 w-6/12 font-bold">
                          <p className="overview-one">{e.book.name}</p>
                        </td>
                        <td className="p-2 w-2/12 ">{e.quantity} cuốn</td>
                        <td className="p-2 w-2/12 ">
                          <p className="text-xs font-bold text-[#49c0f7ec] md:text-base overview-one">
                            {e.book.price.toLocaleString()}đ
                          </p>
                        </td>
                      </tr>
                    ))
                  )}
            </tbody>
          </table>
        </div>
        <div className="lg:w-3/6 w-full bg-[#F6FFFF] rounded-2xl p-1 lg:p-5 shadow-custom shadow-[#c5c3a3]">
          <div>
            <p className="px-5 py-2 text-center bg-[#21bcd8fd] text-white font-bold rounded">
              THÔNG TIN THANH TOÁN
            </p>
            <div className="flex flex-col">
              <ul className="py-2">
                <li className="py-2 flex gap-5">
                  <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-1/2">
                    <label
                      htmlFor="discount"
                      className="pr-3 text-base text-gray-900"
                    >
                      <FaUserCircle></FaUserCircle>
                    </label>
                    <input
                      type="text"
                      name="firstName"
                      id="firstName"
                      placeholder="Họ"
                      onChange={(e) => setFirstName(e.target.value)}
                      defaultValue={firstName}
                      disabled
                      className="w-full"
                    />
                  </div>
                  <div className="flex items-center px-3 rounded-lg border-2 border-blue-200 w-1/2">
                    <input
                      type="text"
                      id="lastName"
                      name="lastName"
                      placeholder="Tên"
                      disabled
                      onChange={(e) => setLastName(e.target.value)}
                      defaultValue={lastName}
                      className="w-full"
                    />
                  </div>
                </li>
                <li className="py-2 flex gap-5">
                  <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-full">
                    <label
                      htmlFor="discount"
                      className="pr-3 text-base text-gray-900"
                    >
                      <FaMapMarkerAlt></FaMapMarkerAlt>
                    </label>
                    <input
                      type="text"
                      id="address"
                      name="address"
                      placeholder="Địa chỉ nhận hàng"
                      disabled
                      onChange={(e) => setShipAddress(e.target.value)}
                      defaultValue={shipAddress}
                      className="w-full"
                    />
                  </div>
                </li>
                <li className="py-2 flex gap-5">
                  <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-full">
                    <label
                      htmlFor="discount"
                      className="pr-3 text-base text-gray-900"
                    >
                      <FaPhone></FaPhone>
                    </label>
                    <input
                      type="number"
                      name="phone"
                      id="phone"
                      disabled
                      placeholder="Số điện thoại"
                      onChange={(e) => setPhone(e.target.value)}
                      defaultValue={phone}
                      className="w-full"
                    />
                  </div>
                </li>
                <li>
                  <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                    <span>Giá </span>
                    <span> {order[0]?.order.sub_price?.toLocaleString()}đ</span>
                  </p>
                </li>
                <li>
                  <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                    <span>Phí vận chuyển</span>
                    <span>
                      {order[0]?.order.shipping_price?.toLocaleString()}đ
                    </span>
                  </p>
                </li>
                <li>
                  <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                    <span>Tổng đơn hàng</span>
                    <b>{order[0]?.order.total_price?.toLocaleString()}đ</b>
                  </p>
                </li>
              </ul>
            </div>
            <div className="py-2 font-semibold text-md flex gap-3 justify-start">
              <p className="w-1/3">Chọn phương thức thanh toán:</p>
              <div className="flex flex-col gap-3 w-2/3">
                <div className="pl-10 flex gap-3 item-center py-2 px-3 rounded-lg border-2 border-blue-200">
                  <input
                    type="radio"
                    id="momo"
                    name="paymentMethod"
                    value="vnpay"
                    onChange={() => setPaymentMethod(0)}
                    checked={paymentMethod === 0 ? true : false}
                    className="mr-2"
                  />
                  <img
                    width={40}
                    src={require("~/assets/images/vnpay.jpg")}
                    alt="VNPAY"
                  />
                  <label className="flex items-center" htmlFor="vnpay">
                    Thanh toán VNPAY <small>(*Lựa chọn thẻ nội địa)</small>
                  </label>
                </div>
                <div className="pl-10 flex gap-5 item-center py-2 px-3 rounded-lg border-2 border-blue-200">
                  <input
                    type="radio"
                    id="late"
                    name="paymentMethod"
                    value="late"
                    onChange={() => setPaymentMethod(1)}
                    checked={paymentMethod === 1 ? true : false}
                    className="mr-2"
                  />
                  <img
                    width={40}
                    src={require("~/assets/images/give.png")}
                    alt="late"
                  />
                  <label className="flex items-center" htmlFor="late">
                    Thanh toán khi nhận hàng
                  </label>
                </div>
              </div>
            </div>
            <div className="w-full flex flex-row gap-5 justify-center items-center relative py-2">
              <div className="absolute right-0">
                {param.id && orders.length > 0 && (
                  <Button
                    className="font-thin text-sm p-2"
                    onClick={() => handleShowCancelOrder()}
                  >
                    <u>Huỷ đơn</u>
                  </Button>
                )}
              </div>
              <div className="text-center">
                <Button
                  type={"submit"}
                  onClick={() => handlePayment()}
                  className="bg-[#21bcd8fd] shadow-md hover:shadow-none shadow-[#5f5e5eb5] text-white text-md md:text-lg font-extrabold p-2"
                >
                  <p>Đặt hàng</p>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {showCancelOrder && (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50">
            <div className=" w-[100vw] relative my-6">
              {/*content*/}
              <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-2/3 lg:w-1/2 bg-[#effcf9] py-12 px-5">
                {/*header*/}
                <div className="flex flex-row items-center w-full lg:w-4/5">
                  <button
                    className="text-xl absolute right-0 top-0 text-red-500 font-bold uppercase px-2 lg:px-6 py-2"
                    type="button"
                    onClick={handleShowCancelOrder}
                  >
                    X
                  </button>
                </div>
                {/*body*/}
                <div className="relative md:p-6 flex-auto overflow-auto max-h-[70vh]">
                  <p className="mx-auto text-center">
                    Bạn có chắc chắn muốn huỷ đơn hàng?
                  </p>
                  <div>
                    <div className="text-center pt-9">
                      <Button
                        type={"submit"}
                        onClick={() => handleCancelOrder()}
                        className="bg-[#d82121fd] shadow-md hover:shadow-none shadow-[#5f5e5eb5] text-white text-md md:text-base font-bold p-2"
                      >
                        <p>Huỷ đơn hàng</p>
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      )}
    </>
  );
}
