export const currentUserSelector = (state) => state.auth.currentUser;
export const currentCartSelector = (state) => state.auth.cart;
export const isLoggedInSelector = (state) => state.auth.isLoggedIn;
export const isVerifiedSelector = (state) => state.auth.isVerified;
export const totalSelector = (state) => state.auth.total;

export const ordersSelector = (state) => state?.orders?.data ?? "";
export const justCreateOderSelector = (state) =>
  state?.orders?.justCreated ?? "";
export const conversationsSelector = (state) =>
  state?.conversations?.data ?? "";
export const conversationsUsersSelector = (state) =>
  state?.conversations?.conversation ?? "";
export const commentsSelector = (state) => state?.comment?.data ?? "";

export const favoriteSelector = (state) => state.favorite.data;
