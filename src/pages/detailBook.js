import Tippy from "@tippyjs/react";
// import WebSocket from "websocket";
import React, { useEffect, useState, useRef } from "react";
import { FaAngleRight, FaHeart, FaStar } from "react-icons/fa";
import { Link, useNavigate, useParams } from "react-router-dom";
import greenBookAPI from "~/api/greenBookAPI";
import RiseLoader from "react-spinners/RiseLoader";

import Card from "./components/card";
import { useDispatch, useSelector } from "react-redux";
import {
  conversationsSelector,
  conversationsUsersSelector,
  currentCartSelector,
  currentUserSelector,
  favoriteSelector,
  isLoggedInSelector,
  ordersSelector,
} from "~/redux/selectors";
import { addCart, updateCart } from "~/redux/reducers/auth";
import { unwrapResult } from "@reduxjs/toolkit";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { addComment } from "~/redux/reducers/comments";
import { debounce } from "lodash";
import Button from "~/components/button";
import { addToFavorite, removeFromFavorite } from "~/redux/reducers/favortite";
import ReactTimeAgo from "react-time-ago";
import { parseISO } from "date-fns";

export default function DetailBook(props) {
  const param = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(isLoggedInSelector);
  const cart = useSelector(currentCartSelector);
  const favorite = useSelector(favoriteSelector);
  const orders = useSelector(ordersSelector);
  const currentUser = useSelector(currentUserSelector);
  const [collapsed, setCollapsed] = useState(true);
  const [publisher, setPublisher] = useState("");
  const [itemBook, setItemBook] = useState([]);
  const [loading, setLoading] = useState(true);
  const [quantity, setQuantity] = useState(1);
  const [recommend, setRecommend] = useState([]);
  const [color, setColor] = useState("#048957");
  const [slice, setSlice] = useState(10);

  const [replyComment, setReplyComment] = useState([]);
  const [showLess, setShowLess] = useState(true);

  const [messageText, setMessageText] = useState("");
  const [rating, setRating] = useState(1);
  const [parentId, setParentId] = useState("");

  const handleAddToFavorite = () => {
    try {
      dispatch(
        addToFavorite({
          id: itemBook.id,
        })
      )
        .then(unwrapResult)
        .then(notify("Đã thêm vào yêu thích"))
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {}
  };

  const Others = () => {
    return <span>Họ</span>;
  };

  const handleRemoveFromFavorite = () => {
    try {
      dispatch(
        removeFromFavorite({
          id: itemBook.id,
        })
      )
        .then(unwrapResult)
        .then(notify("Đã loại bỏ khỏi yêu thích"))
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {}
  };

  const Comment = ({ id, idUser }) => {
    const [showLessReplyComment, setShowLessReplyComment] = useState(true);
    const filteredComments = replyComment.filter((c) => c.parent_id === id);
    const replyContents = filteredComments.map((c) => c.content);
    return (
      <div className="pl-2">
        <p
          className="font-semibold text-sm cursor-pointer p-2"
          onClick={() => setShowLessReplyComment(!showLessReplyComment)}
        >
          {showLessReplyComment ? (
            <span> Xem phản hồi ({filteredComments.length})</span>
          ) : (
            <span> Ẩn phản hồi </span>
          )}
        </p>
        {!showLessReplyComment &&
          replyContents.map((content, index) => (
            <div key={index}>
              <span>
                {toString(currentUser.id_user) === toString(idUser) ? (
                  <b>Bạn đã trả lời</b>
                ) : (
                  <b>Họ đã trả lời</b>
                )}
              </span>
              <p>{content}</p>
            </div>
          ))}
      </div>
    );
  };

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const handleReply = (id) => {
    setParentId(!parentId?.includes(id) ? id : "");
  };

  const handleAddToCart = (status) => {
    if (isLoggedIn) {
      let isBookExist = false;
      let cartItemId = false;
      let quantityUpdate;

      for (let i = 0; i < cart.length; i++) {
        if (cart[i].book_idBook === itemBook.id) {
          cartItemId = cart[i].id;
          quantityUpdate = cart[i].quantity + quantity;
          isBookExist = true;
          break;
        }
      }
      if (isBookExist) {
        dispatch(
          updateCart({
            cartItemId,
            quantity: quantityUpdate,
          })
        )
          .then(unwrapResult)
          .then(setColor("#048957"))
          .then(() => {
            notify("Đã cập nhật vào giỏ!");
          })
          .catch((err) => {
            console.log(err);
          });
        status === 1 &&
          setTimeout(() => {
            navigate("/cart");
          }, 3000);
      } else {
        dispatch(
          addCart({
            bookId: itemBook.id,
            quantity,
          })
        )
          .then(unwrapResult)
          .then(setColor("#048957"))
          .then(() => {
            notify("Đã thêm vào giỏ");
          })
          .catch((err) => {
            console.log(err);
          });
        status === 1 && navigate("/cart");
      }
    } else {
      setColor("#980303");
      notify("Vui lòng đăng nhập");
      setTimeout(() => navigate("/profile"), 1500);
    }
  };

  // For preload spiner
  const override = {
    display: "flex",
    margin: "0 auto",
  };

  const increaseQuantity = () => {
    setQuantity(quantity + 1);
  };

  const decreaseQuantity = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const toggleReadMore = () => {
    setCollapsed(!collapsed);
  };

  const handleSendMessage = () => {
    if (isLoggedIn) {
      const subArray = orders.filter((item) => {
        return (
          item.orderItems &&
          item.orderItems.some((obj) => obj.book_idBook === itemBook.id)
        );
      });
      if (subArray.length > 0) {
        if (parseInt(rating) < 6) {
          setMessageText("");
          setParentId("");
          setRating(1);
          dispatch(
            addComment({
              bookId: itemBook.id,
              content: messageText,
              rate: rating,
              parentId: parentId,
            })
          )
            .then(unwrapResult)
            .catch((err) => {
              console.log(err);
            });
          const debouncedComments = debounce(handleRefreshBook, 1000);
          debouncedComments();
        }
      } else {
        setColor("#980303");
        notify("Bạn chưa mua cuốn sách này!");
      }
    } else {
      navigate("/profile");
    }
  };

  const handleRefreshBook = async () => {
    let response = null;
    response = await greenBookAPI.getBook("", param.name, currentUser.id);
    setItemBook(response.data.data);
    setReplyComment(
      response.data.data.comments.filter((e) => e.parent_id.length > 0 && e)
    );
  };

  const handleKeypress = (e) => {
    if (e.code === "Enter") {
      handleSendMessage();
    }
  };

  // Get Detail book
  useEffect(() => {
    const getBook = async () => {
      try {
        let response = null;
        let responsePublisher = null;
        response = await greenBookAPI.getBook("", param.name, currentUser.id);
        responsePublisher = await greenBookAPI.getPublisher(
          response.data.data.create_by,
          10,
          1
        );
        setItemBook(response.data.data);
        setPublisher(responsePublisher.data.data.publisher);
        setLoading(false);
        setReplyComment(
          response.data.data.comments.filter((e) => e.parent_id.length > 0 && e)
        );
      } catch (error) {
        console.log(error);
      }
    };
    getBook();
  }, [currentUser.id, param.name]);

  function randomDigit() {
    setSlice(Math.floor(Math.random() * 13) + 8);
  }

  useEffect(() => {
    randomDigit();
  }, []);

  // Get List recommend book
  useEffect(() => {
    const getRecommendBook = async () => {
      if (loading !== true) {
        try {
          window.scrollTo(0, 0);
          let responseRecommend;
          responseRecommend = await greenBookAPI.getGenre(
            "",
            itemBook.genres[0].name,
            20,
            2
          );
          setRecommend(responseRecommend.data.data.books);
        } catch (error) {
          console.log(error);
        }
      }
    };
    getRecommendBook();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  return loading ? (
    <div className="grid place-items-center h-[77vh]">
      <RiseLoader
        color="#55ddef"
        loading={loading}
        cssOverride={override}
        className="relative m-auto z-50 flex items-center justify-center flex-row"
        size={50}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
      <div className="fixed inset-0 z-40 bg-[#effcfb]"></div>
    </div>
  ) : (
    <div className="p-2">
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />
      <div className="w-full sm:w-1/2 md:w-3/5 bg-[#21bcd8fd] rounded-r-2xl p-2 mb-5">
        <b className="text-sm lg:text-lg text-white flex flex-row items-center">
          <Link to={"/"}> Sách</Link> {<FaAngleRight></FaAngleRight>}
          <span className="overview-one">{param.name}</span>
        </b>
      </div>
      <div className="max-w-7xl mx-auto flex sm:flex-row sm:items-start items-center flex-col p-1 md:p-5 rounded-lg">
        <div className="sm:w-1/3 p-1 lg:p-5 max-w-lg w-full flex justify-center sm:items-start items-center">
          {itemBook.images && itemBook.images[0] && (
            <img
              className="transition-transform duration-500 hover:scale-110"
              width={300}
              src={itemBook.images[0].url}
              alt={itemBook.name}
            />
          )}
        </div>
        <div className="sm:w-2/3 rounded-2xl p-1 lg:p-5 w-full shadow-md shadow-[#bedcd7e1] ">
          <div className="name flex flex-col p-2 gap-5">
            <div className="flex flex-row flex-wrap gap-5">
              {itemBook.genres.map((e) => (
                <p
                  className="text-[#287ab0] w-fit text-xs font-normal px-2 py-1 rounded-md bg-[#8cddf6c0]"
                  key={e.id}
                >
                  <Link to={`/books/genre/${encodeURIComponent(e.name)}`}>
                    {e.name}
                  </Link>
                </p>
              ))}
            </div>
            <h2 className="text-md sm:text-lg lg:text-xl font-extrabold overview">
              {itemBook.name}
            </h2>
            <div className="flex flex-row justify-between">
              <div className="text-md font-base w-full lg:w-3/5">
                <ul>
                  <li className="py-1">
                    <div className="flex flex-row gap-5 items-center">
                      <span className="flex flex-row">
                        {[...Array(5)].map((_, index) => (
                          <FaStar
                            key={index}
                            className={` ${index} fa fa-star${
                              index < itemBook.rate
                                ? " text-[#f0e632]"
                                : " text-[#4d514d57] "
                            }`}
                          ></FaStar>
                        ))}
                      </span>
                      <span>
                        <a href="#cmt">
                          <u className="font-semibold text-sm cursor-pointer">
                            Bình luận {`(${itemBook.comments.length})`}
                          </u>
                        </a>
                      </span>
                      <span>
                        <a href="#cmt">
                          <u className="font-semibold text-sm cursor-pointer">
                            Thêm đánh giá
                          </u>
                        </a>
                      </span>
                    </div>
                  </li>
                  <li className="py-1 flex justify-between items-center">
                    <span>
                      <b>Độ tuổi: </b>
                      {itemBook.age_limit}
                    </span>
                    <span>
                      <b>Số lượng có sẵn: </b>
                      {itemBook.available_quantity}
                    </span>
                  </li>
                  <li className="py-1 flex justify-between items-center">
                    <span>
                      <b>Bìa:</b> {itemBook.cover_type}
                    </span>
                    <span>
                      <b>Số trang:</b> {itemBook.hand_cover}
                    </span>
                  </li>
                  <li className="py-1 flex justify-between items-center">
                    <span>
                      <b>Đã bán:</b>{" "}
                      {itemBook.sold_quantity == 0
                        ? "2"
                        : itemBook.sold_quantity}
                    </span>
                    <span>
                      <b>Trọng lượng:</b> {itemBook.weight}
                    </span>
                  </li>
                  <li className="flex gap-5 flex-wrap py-1">
                    <b> Tác giả:</b>
                    {itemBook.authors &&
                      itemBook.authors.map((e, i) => (
                        <span className="gap-5" key={i}>
                          <Link to={`/books/author/${e.name}`}>{e.name}</Link>{" "}
                        </span>
                      ))}
                  </li>
                  <li className="flex gap-5 flex-wrap items-center py-1 relative">
                    <b> Nhà xuất bản:</b>
                    <div className="flex flex-row flex-wrap gap-3">
                      <span className="text-[#287ab0] w-fit text-sm font-semibold px-3 py-2 rounded-md bg-[#8cddf6c0]">
                        {publisher.first_name + " " + publisher.last_name}
                      </span>
                      <Tippy content="Liên hệ với nhà xuất bản">
                        <img
                          onClick={() => {
                            notify("Coming Soon");
                          }}
                          className="cursor-pointer"
                          width={"20%"}
                          src={require("../assets/images/chat.png")}
                          alt="chat"
                        />
                      </Tippy>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div className="name flex flex-col p-2 gap-5">
            <div className="flex flex-row justify-between flex-wrap items-center p-2 gap-5">
              <div className="flex flex-row gap-5 fle justify-between items-center">
                <b className="text-sm md:text-lg lg:text-2xl text-[#2eb0d8]">
                  {itemBook.price.toLocaleString()}đ
                </b>
                {parseInt(itemBook.original_price) >
                  parseInt(itemBook.price) && (
                  <span>
                    <strike className="font-medium text-base text-[#6b6a6a]">
                      {itemBook.original_price.toLocaleString()}đ
                    </strike>
                  </span>
                )}
                <p className="flex flex-row justify-between items-center border-2 border-[#bcbcbc3a] rounded-md px-2">
                  <button
                    className="border-r-2 border-[#bcbcbc3a] py-1.5 px-3 font-bold"
                    onClick={() => decreaseQuantity()}
                  >
                    -
                  </button>
                  <span className="border-r-2 border-[#bcbcbc3a] py-1.5 px-3 font-bold">
                    {quantity}{" "}
                  </span>
                  <button
                    className=" py-1.5 px-3 font-bold"
                    onClick={() => increaseQuantity()}
                  >
                    +
                  </button>
                </p>
              </div>
              <div className="flex flex-row justify-end items-center gap-5">
                <Tippy
                  content={`${
                    favorite.some((e) => e === itemBook.id)
                      ? "Bỏ thích"
                      : "Yêu thích"
                  }`}
                  placement="bottom"
                >
                  <p
                    onClick={() => {
                      favorite.some((e) => e === itemBook.id)
                        ? handleRemoveFromFavorite()
                        : handleAddToFavorite();
                    }}
                  >
                    <FaHeart
                      className={`${
                        favorite.some((e) => e === itemBook.id)
                          ? "text-[#fd3668]"
                          : "text-[#e47f98]"
                      } hover:text-[#fd3668] text-xl lg:text-xl`}
                    ></FaHeart>
                  </p>
                </Tippy>
                <Tippy content="Thêm vào giỏ" placement="bottom">
                  <div>
                    <button
                      type={"button"}
                      onClick={() => handleAddToCart(0)}
                      className=" border-2 border-[#21bcd8fd] hover:border-[#40b1c5fd] text-[#21bcd8fd] text-sm md:text-base font-bold  py-1 px-2 rounded-md"
                    >
                      {/* <FaShoppingBasket className="m-1.5" /> */}
                      <p className="m-1.5 md:text-base sm:test-sm text-xs">
                        Thêm vào giỏ hàng
                      </p>
                    </button>
                  </div>
                </Tippy>
                <Tippy content="Mua ngay" placement="bottom">
                  <div>
                    <button
                      type={"button"}
                      onClick={() => handleAddToCart(1)}
                      className=" border-2 bg-[#21bcd8fd] hover:bg-[#40b1c5fd] text-white text-sm md:text-base font-bold  py-1 px-3 rounded-md"
                    >
                      <p className="m-1.5">Mua</p>
                    </button>
                  </div>
                </Tippy>
              </div>
            </div>
            <div
              className={`relative p-1 md:p-3 flex-auto overflow-auto max-h-80 ${
                itemBook.description && itemBook.description.includes(" ")
                  ? "break-words"
                  : "break-all"
              }`}
            >
              <p className="text-md font-extrabold">Mô tả</p>
              <div
                className={`${
                  collapsed
                    ? "max-h-20 overflow-hidden opacity-100 overview-three"
                    : "max-h-full opacity-100"
                } transition-all duration-500`}
              >
                <p
                  dangerouslySetInnerHTML={{ __html: itemBook.description }}
                ></p>
              </div>
            </div>
            <div
              className="text-[#666565] cursor-pointer mt-2 text-center duration-300"
              onClick={toggleReadMore}
            >
              {collapsed ? <b>Xem thêm</b> : <b>Ẩn bớt</b>}
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-row justify-start items-center gap-5 py-5">
        <img
          width={"40"}
          className=""
          src={require("~/assets/images/03.png")}
          alt="best seller"
        />
        <span id="cmt" className="md:text-xl font-bold sm:text-base ">
          Bình luận và đánh giá
        </span>
      </div>
      <div className=" max-w-screen-xl mb-3 mx-auto">
        <div className="border-[#d0fcfcc0] shadow-sm shadow-[#bcd6d2c0]  border-2 rounded px-3 py-2 w-full lg:w-2/3">
          <div className="p-2 flex items-center gap-3">
            <img
              src={
                currentUser?.avatar
                  ? currentUser.avatar
                  : require("../assets/images/author.png")
              }
              alt="user"
              width={"50rem"}
              className="rounded-full"
            />
            <div className="px-3 py-2 w-full flex md:flex-row flex-col items-start md:items-center justify-between ">
              <div className="w-3/6">
                <span className="text-sm">1. Bình luận của bạn.</span>
                <input
                  type="text"
                  value={messageText}
                  placeholder="Bình luận của bạn"
                  onChange={(e) => {
                    setMessageText(e.target.value);
                  }}
                  onKeyDown={(e) => handleKeypress(e)}
                  className="w-full px-2 py-1"
                />
              </div>
              <div className="w-2/6">
                <span className="text-sm">2. Đánh giá của bạn.</span>
                <p className="flex py-1">
                  {[...Array(5)].map((_, index) => (
                    <FaStar
                      key={index}
                      onClick={() => setRating(index + 1)}
                      className={` ${index} fa fa-star${
                        index < rating
                          ? " text-[#f0e632]"
                          : " text-[#4d514d57] "
                      }`}
                    ></FaStar>
                  ))}
                </p>
              </div>
              <div className="w-1/6">
                <Button
                  className="bg-[#3ddcf8] "
                  type="submit"
                  onClick={() => messageText.length > 0 && handleSendMessage()}
                >
                  <span className="px-5 py-3 text-white font-extrabold">
                    Gửi
                  </span>
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="flex-column w-2/3">
          {showLess ? (
            <p
              className="font-semibold text-sm cursor-pointer p-2"
              onClick={() => setShowLess(!showLess)}
            >
              <span> Xem tất cả bình luận ({itemBook.comments.length})</span>
            </p>
          ) : (
            <p
              className="font-semibold text-sm cursor-pointer p-2"
              onClick={() => setShowLess(!showLess)}
            >
              <span>Ẩn bớt bình luận ({itemBook.comments.length})</span>
            </p>
          )}
          {itemBook.comments
            .slice(
              showLess
                ? itemBook.comments.length < 3
                  ? 0
                  : Math.ceil(itemBook.comments.length / 2)
                : 0,
              itemBook.comments.length
            )
            .map((e) => (
              <div key={e.id}>
                {!e.parent_id.length > 0 && (
                  <div className="flex flex-row items-start gap-3 w-full">
                    {currentUser.id_user === e.user_idUser ? (
                      <img
                        src={
                          currentUser?.avatar ??
                          require("../assets/images/author.png")
                        }
                        alt="user"
                        width={"30rem"}
                        className="rounded-full"
                      />
                    ) : (
                      <img
                        src={require("../assets/images/author.png")}
                        alt="user"
                        width={"30rem"}
                        className="rounded-full"
                      />
                    )}
                    <div
                      className={`${
                        currentUser.id === "a"
                          ? "chat-current-user "
                          : "chat-other-user "
                      } w-full lg:w-2/3 flex-col relative`}
                    >
                      <div className="rounded-xl p-2 lg:p-3 shadow-md shadow-[#bedcd7e1] text-break">
                        <span>
                          {currentUser.id_user === e.user_idUser ? (
                            <b>Bạn</b>
                          ) : (
                            <b>
                              <Others id={e.user_idUser}></Others>
                            </b>
                          )}
                        </span>
                        <p>{e.content}</p>
                        {replyComment.filter(
                          (c) => c.parent_id.length > 0 && c.parent_id === e.id
                        ).length > 0 ? (
                          <Comment id={e.id} idUser={e.user_idUser}></Comment>
                        ) : (
                          ""
                        )}

                        <span className="absolute bottom-1 right-1 text-xs ">
                          <ReactTimeAgo
                            date={parseISO(e.created_at ?? "")}
                            locale="vi-VN"
                          />
                        </span>
                      </div>
                      <div className="flex text-xs gap-10 py-2 px-3">
                        <span
                          className="cursor-pointer"
                          onClick={() => handleReply(e.id)}
                        >
                          {parentId === e.id ? (
                            <span className="font-bold text-[#2c99cc]">
                              Đang phản hồi
                            </span>
                          ) : (
                            "Phản hồi"
                          )}
                        </span>
                        <span
                          className="cursor-pointer"
                          onClick={() => alert("Coming soon!")}
                        >
                          Thích
                        </span>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            ))}
        </div>
      </div>
      <div className="flex flex-row justify-start items-center gap-5 py-5">
        <img
          width={"40"}
          className=""
          src={require("~/assets/images/05.png")}
          alt="best seller"
        />
        <span className="md:text-xl font-bold sm:text-base ">
          Có thể bạn quan tâm
        </span>
      </div>
      <div className="max-w-screen-xl mb-5 mx-auto">
        <div
          className="flex flex-wrap -mx-1 lg:-mx-1"
          onClick={() => {
            window.scrollTo(0, 0);
            randomDigit();
          }}
        >
          {recommend &&
            recommend.slice(slice - 5, slice).map((item) => {
              return (
                <Card
                  key={item.id}
                  id={item.id}
                  url={item?.images[0]?.url ?? ""}
                  name={item.name}
                  price={item.price}
                  author={item.cover_type}
                  original_price={item.original_price}
                  rate={item.rate}
                  handleAddToFavorite={handleAddToFavorite}
                  onClick={() => {
                    favorite.some((e) => e === item.id)
                      ? handleRemoveFromFavorite(item.id)
                      : handleAddToFavorite(item.id);
                  }}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
}
