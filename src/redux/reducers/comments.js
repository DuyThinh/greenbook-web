import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import greenBookAPI from "~/api/greenBookAPI";
const commentsSlice = createSlice({
  name: "comments",
  initialState: { status: "idle", data: [] },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addComment.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(addComment.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "idle";
      })
      .addCase(addComment.rejected, (state, action) => {
        state.status = "error";
      });
  },
});

export const addComment = createAsyncThunk(
  "comments/addComment",
  async ({ bookId, content, rate, parentId }) => {
    try {
      const res = await greenBookAPI.addComment(
        bookId,
        content,
        rate,
        parentId
      );
      console.log(res.data);
      if (res.status === 200) {
        const result = res.data;
        return result.data;
      }
    } catch (error) {
      throw error;
    }
  }
);

export default commentsSlice;
