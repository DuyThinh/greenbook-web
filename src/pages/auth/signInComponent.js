import { useFormik } from "formik";
import * as Yup from "yup";
import "./auth.scss";
import Button from "~/components/button";
import { FaAt, FaEye, FaEyeSlash, FaKey } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { login } from "~/redux/reducers/auth";
import { unwrapResult } from "@reduxjs/toolkit";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useEffect, useState } from "react";
import { currentUserSelector, isLoggedInSelector } from "~/redux/selectors";
import greenBookAPI from "~/api/greenBookAPI";
import { getOrders } from "~/redux/reducers/orders";

const SignInForm = (title) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(currentUserSelector);
  const isLogged = useSelector(isLoggedInSelector);
  const [color, setColor] = useState("#980303");
  const [showVerify, setShowVerify] = useState(false);
  const [emailForgot, setEmailForgot] = useState("");
  const [showForgotPassword, setShowForgotPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [update, setUpdate] = useState(0);
  window.scrollTo(0, 0);

  const checkError = (res) => {
    if (currentUser?.firstName && currentUser?.lastName) {
    } else if (
      currentUser === "User is not verified. Email re-confirmed is sent"
    ) {
      setShowVerify(true);
      setColor("#980303");
      notify(res);
    } else if (res.length > 0) {
      setColor("#980303");
      notify(res);
    }
  };

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: async (values) => {
      if (values) {
        const user = {
          email: values?.email,
          password: values?.password,
        };
        dispatch(login(user))
          .then(unwrapResult)
          .then(setUpdate(!update))
          .then((res) => checkError(res))
          .catch((err) => {
            notify(err.message);
          });
      } else {
        console.log("Có biến cmnr!");
      }
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .required("Bắt buộc!")
        .matches(
          /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
          "Vui lòng nhập đúng định dạng email!"
        ),
      password: Yup.string()
        .required("Bắt buộc!")
        .min(4, "Mật khẩu tối thiểu phải có 4 ký tự!"),
    }),
  });

  const schema = Yup.object().shape({
    emailForgot: Yup.string()
      .email("Vui lòng nhập một địa chỉ email hợp lệ")
      .required("Vui lòng nhập địa chỉ email"),
  });

  const handlForgetPassword = async () => {
    try {
      await schema.validate({ emailForgot });
      let response = null;
      let userEmail = emailForgot;
      response = await greenBookAPI.forgotPassword(userEmail);
      if (response.status === 200) {
        if (response.data.message == "Send email reset password successfully") {
          setShowForgotPassword(false);
          setEmailForgot("");
          notify(response.data.message);
        } else {
          setColor("#980303");
          notify(response.data.message);
        }
      } else {
        setColor("#980303");
        notify(`Mã lỗi ${response.status}`);
      }
    } catch (error) {
      notify(error.message); // Lưu thông báo lỗi vào state error
    }
  };

  useEffect(() => {
    isLogged && dispatch(getOrders());
  }, [isLogged]);

  return (
    <>
      {title.title === "Sign In" ? (
        <section className="bg-[#fceab8ea] w-5/6 md:w-2/3 lg:w-3/6 mx-auto rounded-lg p-3 flex flex-col justify-center items-center">
          <h3 className="font-semibold text-lg p-3">Đăng nhập</h3>
          <ToastContainer
            toastStyle={{
              backgroundColor: "#b8fcf6ea",
              color: color,
              marginTop: "10vh",
            }}
          />
          <form className="w-full lg:w-1/2" onSubmit={formik.handleSubmit}>
            <div className="p-2">
              <label>
                <FaAt></FaAt>
              </label>
              <input
                onChange={formik.handleChange}
                type="email"
                autoComplete="off"
                value={formik.values.email}
                name="email"
                id="email"
                className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-full bg-[#fdf9e15e]"
                placeholder="Email"
                required
              />
              {formik.errors && (
                <p className="text-red-500 text-sm">{formik.errors.email}</p>
              )}
            </div>

            <div className="p-2">
              <label>
                <FaKey></FaKey>
              </label>
              <div className="relative flex items-center">
                <input
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  type={`${showPassword ? "text" : "password"}`}
                  autoComplete="off"
                  name="password"
                  id="password"
                  className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-full  bg-[#fdf9e15e]"
                  placeholder="Password"
                  required
                />
                {formik.values.password.length > 0 && (
                  <span className="absolute right-1">
                    {showPassword ? (
                      <FaEye onClick={() => setShowPassword(false)}></FaEye>
                    ) : (
                      <FaEyeSlash
                        onClick={() => setShowPassword(true)}
                      ></FaEyeSlash>
                    )}
                  </span>
                )}
              </div>
              {formik.errors && (
                <p className="text-red-500 text-sm">{formik.errors.password}</p>
              )}
            </div>
            <div className="form-group px-2">
              <div className="custom-control custom-checkbox">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id="customCheck1"
                />
                &nbsp;
                <label className="custom-control-label" htmlFor="customCheck1">
                  <small> Lưu mật khẩu</small>
                </label>
              </div>
            </div>

            <div className="text-center pt-8 flex flex-col">
              <Button
                type="submit"
                className="bg-[#21bcd8fd] w-50 rounded-xl border-2 border-[#16aec9c9] hover:shadow-md shadow-sm shadow-[#99fad2] px-5 py-2 text-white"
              >
                Đăng nhập
              </Button>
            </div>

            <div
              className="forgot-password text-center cursor-pointer"
              onClick={() => setShowForgotPassword(true)}
            >
              <small>Quên mật khẩu?</small>
            </div>
          </form>
        </section>
      ) : (
        <>
          <h2>Ohhh....Something wrong!</h2>
        </>
      )}
      {showVerify ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-2/3 lg:w-1/2 bg-[#fdf2e8] outline-none focus:outline-none py-12 px-5">
              {/*header*/}
              <div className="flex flex-col gap-1 items-center justify-center border-blue-100 border-2 rounded-lg w-full px-2">
                <p className="lg:text-base md:text-sm text-xs font-bold opacity-90 text-center">
                  Bạn chưa Verify tài khoản, GreenBook đã gửi lại mã xác thực
                  đến email của bạn.
                </p>
                <p className="lg:text-base md:text-sm text-xs font-bold opacity-90 text-center">
                  Vui lòng kiểm tra hòm thư để xác thực tài khoản của bạn!
                </p>
              </div>
              {/*body*/}
              <div className="md:p-6 flex-auto overflow-auto mx-auto">
                <Button
                  name={"Home"}
                  id={"Home"}
                  type={"button"}
                  onClick={() => setShowVerify(false)}
                  className="bg-[#04d6d6] text-white text-md md:text-base font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
                >
                  <p>Tôi hiểu rồi!</p>
                </Button>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
      {showForgotPassword ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-2/3 lg:w-1/2 bg-[#fdf2e8] outline-none focus:outline-none py-12 px-5">
              {/*header*/}
              <div className="flex flex-col gap-1 items-center justify-center border-blue-100 border-2 rounded-lg w-full px-2">
                <p className="lg:text-base md:text-sm text-xs font-bold opacity-90 text-center">
                  Sau khi kiểm tra, chúng tôi sẽ gửi mật khẩu mới đến Email của
                  bạn.
                </p>
                <p className="lg:text-base md:text-sm text-xs font-bold opacity-90 text-center">
                  Vui lòng kiểm tra hòm thư để nhận mật khẩu mới!
                </p>
                <div className="py-3 w-10/12 md:w-1/2">
                  <label>
                    <FaAt></FaAt>
                  </label>
                  <div className="w-full flex flex-row items-center border-b-2 border-[#695308] bg-[#fcecc18d]">
                    <input
                      value={emailForgot}
                      onChange={(e) => setEmailForgot(e.target.value)}
                      type="email"
                      autoComplete="off"
                      name="emailForgot"
                      id="emailForgot"
                      className="sm:text-base text-sm px-3 py-2"
                      placeholder="Email"
                      required
                    />
                  </div>
                </div>
              </div>
              {/*body*/}
              <div className="p-5 flex-auto overflow-auto mx-auto space-x-5">
                <Button
                  name={"cancel"}
                  id={"cancel"}
                  type={"button"}
                  onClick={() => {
                    setShowForgotPassword(false);
                    setEmailForgot("");
                  }}
                  className="text-black text-md md:text-base font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
                >
                  <p className="text-sm">Huỷ</p>
                </Button>
                <Button
                  name={"Home"}
                  id={"Home"}
                  type={"button"}
                  onClick={() => {
                    handlForgetPassword();
                  }}
                  className="bg-[#04d6d6] text-white text-md md:text-base font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
                >
                  <p>Tôi hiểu rồi!</p>
                </Button>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default SignInForm;
