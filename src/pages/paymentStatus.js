import React, { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

export default function PaymentStatus() {
  const navigate = useNavigate();
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  // const orderId = searchParams.get("orderId");
  const status = searchParams.get("status");

  useEffect(() => {
    if (status === "Successfully") {
      setTimeout(() => navigate("/orders"), 3000);
    } else {
      // handle at here
      setTimeout(() => navigate("/orders"), 3000);
    }
  }, []);

  return (
    <>
      <div className="max-w-8xl mx-auto flex lg:flex-row flex-col p-1 md:p-5 rounded-lg gap-5">
        <div className="lg:w-5/6 mx-auto w-full bg-[#F6FFFF] rounded-2xl p-1 lg:p-5 shadow-custom shadow-[#c5c3a3]">
          <p
            className={`px-5 py-2 text-center ${
              status === "Successfully" ? "bg-[#21bcd8fd]" : "bg-[#d82d21fd]"
            } text-white font-bold rounded`}
          >
            {status === "Successfully"
              ? "Đặt hàng thành công"
              : "Đặt hàng thất bại"}
          </p>
        </div>
      </div>
    </>
  );
}
