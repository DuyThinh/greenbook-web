import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import greenBookAPI from "~/api/greenBookAPI";
const conversationsSlice = createSlice({
  name: "conversation",
  initialState: { status: "idle", data: [], conversation: [] },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(createConversation.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(createConversation.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "idle";
      })
      .addCase(getConversation.fulfilled, (state, action) => {
        state.conversation = action.payload;
        state.status = "idle";
      })
      .addCase(createConversation.rejected, (state, action) => {
        state.status = "error";
      });
  },
});

export const createConversation = createAsyncThunk(
  "conversation/createConversation",
  async ({ id }) => {
    try {
      const res = await greenBookAPI.createConversation(id);
      console.log(res.data);
      if (res.status === 200) {
        const result = res.data;
        return result.data;
      }
    } catch (error) {
      throw error;
    }
  }
);
export const getConversation = createAsyncThunk(
  "conversation/getConversation",
  async ({ id }) => {
    try {
      const res = await greenBookAPI.getConversation(id);
      if (res.status === 200) {
        const result = res.data;
        return result.data;
      }
    } catch (error) {
      throw error;
    }
  }
);
export const postMessage = createAsyncThunk(
  "conversation/postMessage",
  async ({ id, content }) => {
    try {
      const res = await greenBookAPI.postMessage(id, content);
      console.log(res.data);
      if (res.status === 200) {
        const result = res.data;
        return result.data;
      }
    } catch (error) {
      throw error;
    }
  }
);

export default conversationsSlice;
