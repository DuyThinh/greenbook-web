import React, { useEffect, useState, Fragment } from "react";
import Banner from "~/components/banner";
import Card from "./components/card";
// import { Link } from "react-router-dom";
import greenBookAPI from "../api/greenBookAPI";
import Button from "~/components/button";
import Tippy from "@tippyjs/react";
import RiseLoader from "react-spinners/RiseLoader";
import { useDispatch, useSelector } from "react-redux";
import { addToFavorite, removeFromFavorite } from "~/redux/reducers/favortite";
import { unwrapResult } from "@reduxjs/toolkit";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { favoriteSelector, isLoggedInSelector } from "~/redux/selectors";
import { getOrders } from "~/redux/reducers/orders";

export default function Home() {
  const dispatch = useDispatch();
  const favorite = useSelector(favoriteSelector);
  const isLogged = useSelector(isLoggedInSelector);

  const [itemBook, setItemBook] = useState([]);
  const [itemBookNewest, setItemBookNewest] = useState([]);
  const [itemBookBestSeller, setItemBookBestSeller] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(20);
  const [loading, setLoading] = useState(true);
  const [color, setColor] = useState("#048957");

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const override = {
    display: "flex",
    margin: "0 auto",
  };

  const handleAddToFavorite = async (id) => {
    try {
      await dispatch(addToFavorite({ id })).then(unwrapResult);
      notify("Đã thêm vào yêu thích");
    } catch (error) {
      console.log(error);
    }
  };

  const handleRemoveFromFavorite = (id) => {
    try {
      dispatch(
        removeFromFavorite({
          id: id,
        })
      )
        .then(unwrapResult)
        .then(notify("Đã loại bỏ khỏi yêu thích"))
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {}
  };

  const loadMore = async () => {
    setPage(page + 1);
  };
  useEffect(() => {
    const handleLoadMore = async () => {
      let response = null;
      response = await greenBookAPI.getBooks(limit, page);
      setItemBook([...itemBook, ...response.data.data]);
    };

    handleLoadMore();
  }, [page]);

  // Get book, genres, genres's book
  useEffect(() => {
    const getBook = async () => {
      try {
        const response = await greenBookAPI.getBooks(limit, page);
        const responseBestSeller = await greenBookAPI.getBooksBestSeller(5, 1);
        const responseNewest = await greenBookAPI.getBooksHotStock(5, 1);
        setItemBook(response.data.data);
        setItemBookBestSeller(responseBestSeller.data.data);
        setItemBookNewest(responseNewest.data.data);
        setLoading(false);
      } catch (error) {
        console.log(error);
      }
    };
    getBook();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isLogged) {
      try {
        dispatch(getOrders())
          .then(unwrapResult)
          .catch((err) => {
            console.log(err.message);
          });
      } catch (error) {
        console.log(error);
      }
    }
  }, []);

  return loading ? (
    <div className="grid place-items-center h-[75vh]">
      <RiseLoader
        color="#55ddef"
        loading={loading}
        cssOverride={override}
        className="relative m-auto z-50 flex items-center justify-center flex-row"
        size={40}
        aria-label="Loading Spinner"
        data-testid="loader"
      />

      <div className="fixed inset-0 z-40 bg-[#effcfb]"></div>
    </div>
  ) : (
    <>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />
      <Banner></Banner>
      <div className="p-2">
        <div className="flex flex-row justify-start items-center gap-5 py-5">
          <img
            width={"40"}
            className=""
            src={require("~/assets/images/01.png")}
            alt="best seller"
          />
          <span
            id="bestSeller"
            className="md:text-xl font-semibold sm:text-base "
          >
            Bán chạy
          </span>
        </div>
        <div className="max-w-screen-xl mb-5 mx-auto">
          <div className="flex flex-wrap -mx-1 lg:-mx-1">
            {itemBookBestSeller &&
              itemBookBestSeller.map((item) => {
                return (
                  <Card
                    key={item.id}
                    id={item.id}
                    url={item.images[0].url}
                    name={item.name}
                    price={item.price}
                    author={item.authors[0].name}
                    original_price={item.original_price}
                    rate={item.rate}
                    onClick={() => {
                      favorite.some((e) => e === item.id)
                        ? handleRemoveFromFavorite(item.id)
                        : handleAddToFavorite(item.id);
                    }}
                  />
                );
              })}
          </div>
        </div>
        <div className="flex flex-row justify-start items-center gap-5 py-5">
          <img
            width={"40"}
            className=""
            src={require("~/assets/images/04.png")}
            alt="best seller"
          />
          <span id="newest" className="md:text-xl font-semibold sm:text-base ">
            Giảm giá
          </span>
        </div>
        <div className="max-w-screen-xl mb-5 mx-auto">
          <div className="flex flex-wrap -mx-1 lg:-mx-1">
            {itemBookNewest &&
              itemBookNewest.map((item) => {
                return (
                  <Card
                    key={item.id}
                    id={item.id}
                    url={item.images[0].url}
                    name={item.name}
                    price={item.price}
                    author={item.authors[0].name}
                    original_price={item.original_price}
                    rate={item.rate}
                    onClick={() => {
                      favorite.some((e) => e === item.id)
                        ? handleRemoveFromFavorite(item.id)
                        : handleAddToFavorite(item.id);
                    }}
                  />
                );
              })}
          </div>
        </div>
        <div className="flex flex-row justify-start items-center gap-5 py-5">
          <img
            width={"40"}
            className=""
            src={require("~/assets/images/05.png")}
            alt="best seller"
          />
          <span id="newest" className="md:text-xl font-semibold sm:text-base ">
            Có thể bạn thích
          </span>
        </div>
        <div className="max-w-screen-xl mb-5 mx-auto">
          <div className="flex flex-wrap -mx-1 lg:-mx-1">
            {itemBook &&
              itemBook.map((item) => {
                return (
                  <Card
                    key={item.id}
                    id={item.id}
                    url={item.images[0].url}
                    name={item.name}
                    price={item.price}
                    original_price={item.original_price}
                    author={item.authors[0].name}
                    rate={item.rate}
                    onClick={() => {
                      favorite.some((e) => e === item.id)
                        ? handleRemoveFromFavorite()
                        : handleAddToFavorite();
                    }}
                  />
                );
              })}
          </div>
          <Tippy placement="bottom" content="Xem thêm">
            <div className="flex w-full justify-center items-end">
              <Button
                className="bg-[#9cecfab9] text-[#1f878a]"
                title=""
                type={"button"}
                onClick={loadMore}
              >
                <span className="md:font-bold text-sm lg:text-base ">
                  {"<<"} Xem thêm {">>"}
                </span>
              </Button>
            </div>
          </Tippy>
        </div>
      </div>
    </>
  );
}
