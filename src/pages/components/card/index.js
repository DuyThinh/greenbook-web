import React, { useState } from "react";
import { FaPlus, FaRegHeart, FaStar } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";
import { useDispatch, useSelector } from "react-redux";
import {
  currentCartSelector,
  favoriteSelector,
  isLoggedInSelector,
} from "~/redux/selectors";
import { unwrapResult } from "@reduxjs/toolkit";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { addCart, updateCart } from "~/redux/reducers/auth";

export default function Card(props) {
  const favorite = useSelector(favoriteSelector);
  const isLoggedIn = useSelector(isLoggedInSelector);
  const dispatch = useDispatch();
  const cart = useSelector(currentCartSelector);
  const [color, setColor] = useState("#048957");
  const navigate = useNavigate();

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const handleAddToCart = (itemBook) => {
    if (isLoggedIn) {
      let isBookExist = false;
      let cartItemId = false;
      let quantityUpdate;

      for (let i = 0; i < cart.length; i++) {
        if (cart[i].book_idBook === itemBook) {
          cartItemId = cart[i].id;
          quantityUpdate = cart[i].quantity + 1;
          isBookExist = true;
          break;
        }
      }
      if (isBookExist) {
        console.log(cartItemId, quantityUpdate);
        dispatch(
          updateCart({
            cartItemId,
            quantity: quantityUpdate,
          })
        )
          .then(unwrapResult)
          .then(setColor("#048957"))
          .then(() => {
            notify("Đã cập nhật vào giỏ!");
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        console.log(itemBook, "1");
        dispatch(
          addCart({
            bookId: itemBook,
            quantity: 1,
          })
        )
          .then(unwrapResult)
          .then(setColor("#048957"))
          .then(() => {
            notify("Đã thêm vào giỏ");
          })
          .catch((err) => {
            console.log(err);
          });
      }
    } else {
      setColor("#980303");
      notify("Vui lòng đăng nhập");
      setTimeout(() => navigate("/profile"), 1000);
    }
  };

  return (
    <>
      <div
        key={props.id}
        className="transition-transform duration-500 hover:scale-105 my-1 p-2 sm:px-7 w-1/2 md:w-1/3 lg:my-4 lg:px-4 lg:w-1/5 2xl:w-1/5 relative"
      >
        <article className="overflow-hidden rounded-lg shadow-lg shadow-custom h-full p-1.5 ">
          <Link to={`/detailBook/${encodeURIComponent(props.name)}`}>
            <img
              alt={props.name}
              className="block max-h-70 object-cover w-11/12 mx-auto object-center rounded-md"
              src={
                props.url ? props.url : require("~/assets/images/author.png")
              }
            />
          </Link>
          <div className="mt-1 h-36">
            <header className="text-black flex flex-col items-start gap-1 justify-between leading-tight p-1 md:p-4 absolute bottom-2 transform translate-x-[-1%] w-5/6">
              <h1 className="text-base overview h-11">
                <Link to={`/detailBook/${encodeURIComponent(props.name)}`}>
                  <Tippy placement="bottom" content={props.name}>
                    <b className="overview">{props.name}</b>
                  </Tippy>
                </Link>
              </h1>
              <ul className="w-full">
                {props.author ? (
                  <li className="flex flex-row items-center justify-between py-2">
                    <Tippy placement="bottom" content={props.author}>
                      <p className="text-sm overview-one h-5 max-w-[65%]">
                        {props.author}
                      </p>
                    </Tippy>
                    <span className="flex flex-row">
                      {[...Array(5)].map((_, index) => (
                        <FaStar
                          key={index}
                          className={`text-xs ${index} fa fa-star${
                            index < props.rate
                              ? " text-[#f0e632]"
                              : " text-[#4d514d57] "
                          }`}
                        ></FaStar>
                      ))}
                    </span>
                  </li>
                ) : (
                  <li className="flex flex-row items-center justify-between py-2">
                    <Tippy placement="bottom" content={props.genres}>
                      <p className="text-sm overview-one h-5">{props.genres}</p>
                    </Tippy>
                    <span className="flex flex-row">
                      {[...Array(5)].map((_, index) => (
                        <FaStar
                          key={index}
                          className={`text-xs ${index} fa fa-star${
                            index < props.rate
                              ? " text-[#f0e632]"
                              : " text-[#4d514d57] "
                          }`}
                        ></FaStar>
                      ))}
                    </span>
                  </li>
                )}
                <li className="flex flex-row items-start justify-between py-2 w-ful">
                  <div>
                    <b className="text-sm text-[#2eb0d8] pr-2">
                      {props.price.toLocaleString()}đ
                    </b>
                    {parseInt(props.original_price) > parseInt(props.price) && (
                      <span>
                        {
                          <strike className="text-xs text-[#6b6a6a]">
                            {props.original_price.toLocaleString()}đ
                          </strike>
                        }
                      </span>
                    )}
                  </div>
                  <div className="flex gap-2 items-center">
                    <Tippy
                      placement="bottom"
                      content={`${
                        favorite.some((e) => e === props.id)
                          ? "Bỏ thích"
                          : "Yêu thích"
                      }`}
                    >
                      <div
                        className="flex gap-2"
                        onClick={props.onClick ? () => props.onClick() : null}
                      >
                        {props.onClick ? (
                          <>
                            <FaRegHeart
                              className={`${
                                favorite.some((e) => e === props.id)
                                  ? "text-[#fd3556] hover:text-black"
                                  : " hover:text-[#fd3556]"
                              }`}
                            ></FaRegHeart>
                          </>
                        ) : (
                          <Link
                            to={`/detailBook/${encodeURIComponent(props.name)}`}
                          >
                            <FaRegHeart className="hover:text-[#fd3556]"></FaRegHeart>
                          </Link>
                        )}
                      </div>
                    </Tippy>
                    {isLoggedIn && (
                      <Tippy content="Thêm vào giỏ hàng" placement="bottom">
                        <button
                          onClick={() => handleAddToCart(props.id)}
                          className={
                            "bg-[#24eef5]  py-1 px-2 rounded-lg text-sm"
                          }
                        >
                          <FaPlus className="text-white"></FaPlus>
                        </button>
                      </Tippy>
                    )}
                  </div>
                </li>
              </ul>
            </header>
          </div>
        </article>
      </div>
    </>
  );
}
