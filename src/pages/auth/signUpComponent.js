import * as Yup from "yup";
import moment from "moment";
import Button from "~/components/button";
import { useFormik } from "formik";
import { FaAt, FaBirthdayCake, FaKey, FaUserAlt } from "react-icons/fa";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { GoogleLogin } from "@react-oauth/google";
import jwt_decode from "jwt-decode";
import { useDispatch } from "react-redux";
import { register } from "~/redux/reducers/auth";
import { unwrapResult } from "@reduxjs/toolkit";
import { useEffect, useState } from "react";
import DatePicker from "react-datepicker";
import { parse, differenceInYears } from "date-fns";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./auth.scss";
import "react-datepicker/dist/react-datepicker.css";

const SignUpForm = (props) => {
  const [startDate, setStartDate] = useState(new Date());
  const [avatar, setAvatar] = useState("");
  const [fullName, setFullName] = useState();
  const [color, setColor] = useState("#980303");
  const [showVerify, setShowVerify] = useState(false);
  const dispatch = useDispatch();

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  useEffect(() => {
    const formattedDate = moment(startDate).format("YYYY-MM-DD");
    const parsedDate = parse(formattedDate, "yyyy-MM-dd", new Date());
    const currentDate = new Date();
    const age = differenceInYears(currentDate, parsedDate);
    const isValidAge = age >= 1 && age <= 123;

    isValidAge
      ? formik.setFieldValue("dateOfBirth", formattedDate)
      : formik.setErrors({ dateOfBirth: "Vui lòng nhập đúng định dạng!" });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [startDate]);

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      email: "",
      password: "",
      confirmedPassword: "",
    },
    onSubmit: async (values) => {
      try {
        if (values) {
          // Get random letter
          const getRandomLetter = () =>
            "abcdefghijklmnopqrstuvwxyz"[Math.floor(Math.random() * 26)];
          const randomLetter = getRandomLetter();

          const user = {
            email: values.email,
            password: values.password,
            avatar: avatar
              ? avatar
              : `https://robohash.org/${randomLetter}?set=set5&bgset=&size=400x400`,
            first_name: values.firstName,
            last_name: values.lastName,
            date_of_birth: values.dateOfBirth,
            mobile: "",
            ship_address: "",
            default_address: "",
          };
          await dispatch(register(user))
            .then(unwrapResult)
            .then(() => {
              formik.setFieldValue("firstName", "");
              formik.setFieldValue("lastName", "");
              formik.setFieldValue("email", "");
              formik.setFieldValue("password", "");
              formik.setFieldValue("password", "");
              setShowVerify(true);
              setColor("#048957");
              notify("Tạo thành công!");
            })
            .catch((err) => {
              setColor("#980303");
              notify(err?.message ? err.message : err);
              console.log(err?.message ? err.message : err);
            });
        } else {
          setColor("#980303");
          notify("Đã có sự cố vui lòng liên hệ Owner!");
        }
      } catch (error) {
        setColor("#980303");
        notify("Vui lòng kiểm tra lại!");
        console.log(error);
      }
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .required("Bắt buộc!")
        .matches(
          /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/,
          "Họ người dùng ít nhất 1 ký tự và không có ký tự đặc biệt!"
        ),
      lastName: Yup.string()
        .required("Bắt buộc!")
        .matches(
          /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/,
          "Tên người dùng ít nhất 2 ký tự và không có ký tự đặc biệt!"
        ),
      dateOfBirth: Yup.date().required("vui lòng điền ngày sinh !"),
      email: Yup.string()
        .required("Bắt buộc!")
        .matches(
          /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
          "Vui lòng nhập đúng định dạng email!"
        ),
      password: Yup.string()
        .required("Bắt buộc!")
        .matches(
          /^(?=.*[A-Z]).{4,}$/,
          "Mật khẩu ít nhất 4 ký tự và một chữ in hoa!"
        ),
      confirmedPassword: Yup.string()
        .required("Bắt buộc!")
        .oneOf([Yup.ref("password"), null], "Mật khẩu không khớp!"),
    }),
  });

  // Get info FromGoogle
  const getInfoFromGoogle = (details) => {
    setAvatar(details.picture);
    setFullName(details.name);
    formik.setFieldValue("firstName", details.family_name);
    formik.setFieldValue("lastName", details.given_name);
    formik.setFieldValue("email", details.email);
  };

  return (
    <>
      {props.title === "Sign Up" ? (
        <section className="bg-[#fceab8ea] w-5/6 md:w-2/3 lg:w-3/6 mx-auto rounded-lg p-3 flex flex-col justify-center items-center">
          <h3 className="font-semibold text-lg p-3">Đăng ký</h3>
          {fullName && <p>Xin chào {fullName}</p>}
          <ToastContainer
            toastStyle={{
              backgroundColor: "#b8fcf6ea",
              color: color,
              marginTop: "10vh",
            }}
          />
          <form className="w-full lg:w-1/2" onSubmit={formik.handleSubmit}>
            <div className="p-2">
              <label>
                <FaUserAlt></FaUserAlt>
              </label>
              <div className="flex justify-around gap-2">
                <input
                  value={formik.values.firstName}
                  onChange={formik.handleChange}
                  name="firstName"
                  id="firstName"
                  type="text"
                  autoComplete="off"
                  className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-1/2 bg-[#fdf9e15e]"
                  placeholder="First name"
                  required
                />
                <input
                  value={formik.values.lastName}
                  onChange={formik.handleChange}
                  name="lastName"
                  id="lastName"
                  type="text"
                  autoComplete="off"
                  className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-1/2 bg-[#fdf9e15e]"
                  placeholder="Last name"
                  required
                />
              </div>
              {formik.errors && (
                <p className="text-red-500 text-sm">
                  {formik.errors.lastName ?? formik.errors.firstName}
                </p>
              )}
            </div>
            <div className="p-2">
              <label>
                <FaBirthdayCake></FaBirthdayCake>
              </label>
              <DatePicker
                className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-full bg-[#fdf9e15e]"
                selected={startDate}
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                maxDate={new Date()}
                onChange={(date) => {
                  setStartDate(date);
                }}
              />
              {formik.touched.dateOfBirth && formik.errors.dateOfBirth && (
                <p className="text-red-500 text-sm">
                  {formik.errors.dateOfBirth}
                </p>
              )}
            </div>
            <div className="p-2">
              <label>
                <FaAt></FaAt>
              </label>
              <input
                value={formik.values.email}
                onChange={formik.handleChange}
                name="email"
                id="email"
                type="email"
                autoComplete="off"
                className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-full bg-[#fdf9e15e]"
                placeholder="Email"
                required
              />
              {formik.errors && (
                <p className="text-red-500 text-sm">{formik.errors.email}</p>
              )}
            </div>
            <div className="p-2">
              <label>
                <FaKey></FaKey>
              </label>
              <input
                value={formik.values.password}
                onChange={formik.handleChange}
                name="password"
                id="password"
                type="password"
                autoComplete="off"
                className="sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-full bg-[#fdf9e15e]"
                placeholder="Password"
                required
              />
              {formik.errors && (
                <p className="text-red-500 text-sm">{formik.errors.password}</p>
              )}
            </div>
            <div className="p-2">
              <input
                value={formik.values.confirmedPassword}
                onChange={formik.handleChange}
                name="confirmedPassword"
                id="confirmedPassword"
                required
                type="password"
                autoComplete="off"
                className={`sm:text-base text-sm px-3 py-2 border-b-2 border-[#695308] w-full bg-[#fdf9e15e]`}
                placeholder="Password again"
              />
              {formik.errors && (
                <p className="text-red-500 text-sm">
                  {formik.errors.confirmedPassword}
                </p>
              )}
            </div>

            <div className="text-center pt-8">
              <Button
                type="submit"
                className="bg-[#21bcd8fd] w-50 rounded-xl border-2 border-[#16aec9c9] hover:shadow-md shadow-sm shadow-[#99fad2] px-5 py-2 text-white"
              >
                Đăng ký
              </Button>
              <p className="pt-5">
                <small className="text-center">hoặc</small>
              </p>
            </div>
            <div className="flex flex-row justify-center mx-auto p-5">
              <GoogleOAuthProvider clientId="840711155369-41g9h616u4aee4bdqiempqjcmlufgqdn.apps.googleusercontent.com">
                <GoogleLogin
                  onSuccess={(credentialResponse) => {
                    const details = jwt_decode(credentialResponse.credential);
                    getInfoFromGoogle(details);
                    // console.log(credentialResponse);
                  }}
                  onError={() => {
                    console.log("Login Failed");
                  }}
                />
              </GoogleOAuthProvider>
            </div>
          </form>
        </section>
      ) : (
        <>
          <h2> Ohhh shit....Something error!</h2>
        </>
      )}
      {showVerify ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-2/3 lg:w-1/2 bg-[#fdf2e8] outline-none focus:outline-none py-12 px-5">
              {/*header*/}
              <div className="flex flex-col gap-1 items-center justify-center border-blue-100 border-2 rounded-lg w-full px-2">
                <p className="text-base font-bold opacity-90 text-center">
                  GreenBook đã gửi mã xác thực đến email của bạn.
                </p>
                <p className="text-base font-bold opacity-90 text-center">
                  Vui lòng kiểm tra hòm thư để xác thực tài khoản của bạn!
                </p>
              </div>
              {/*body*/}
              <div className="md:p-6 flex-auto overflow-auto mx-auto">
                <Button
                  name={"Home"}
                  id={"Home"}
                  type={"button"}
                  onClick={() => {
                    setShowVerify(false);
                    props.onSuccess();
                  }}
                  className="bg-[#04d6d6] text-white text-md md:text-base font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
                >
                  <p>Tôi hiểu rồi!</p>
                </Button>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default SignUpForm;
