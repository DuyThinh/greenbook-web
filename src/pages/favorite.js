import React, { useEffect, useState } from "react";
import { FaAngleRight } from "react-icons/fa";
import { Link, useParams, useNavigate } from "react-router-dom";
import greenBookAPI from "~/api/greenBookAPI";
import Card from "./components/card";
import Button from "~/components/button";
import Tippy from "@tippyjs/react";
import { useDispatch, useSelector } from "react-redux";
import { currentUserSelector, favoriteSelector } from "~/redux/selectors";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { removeFromFavorite } from "~/redux/reducers/favortite";
import { unwrapResult } from "@reduxjs/toolkit";

export default function Books() {
  const [color, setColor] = useState("#048957");
  const [itemBook, setItemBook] = useState([]);
  const favorite = useSelector(favoriteSelector);
  const currentUser = useSelector(currentUserSelector);
  const [page, setPage] = useState(1);
  const limit = 10;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const param = useParams();
  // console.log("param", param);

  const handleRemoveFromFavorite = (id) => {
    try {
      dispatch(
        removeFromFavorite({
          id: id,
        })
      )
        .then(unwrapResult)
        .then(notify("Đã loại bỏ khỏi yêu thích"))
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {}
  };

  // For alert
  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  useEffect(() => {
    if (favorite.length !== 0) {
      const requests = favorite.map((id) =>
        greenBookAPI.getBook(id, "", currentUser.id_user)
      );
      // send all request
      Promise.all(requests)
        .then((responses) => {
          let getBooks = [];
          responses.forEach((response) => {
            if (parseInt(response.status) === 200) {
              if (response.data.data && response.data.data) {
                getBooks = [...getBooks, response.data.data];
                setItemBook(getBooks);
              }
            } else {
              setColor("#980303");
              notify("Đã xảy ra lỗi, vui lòng thủ lại!");
              console.log("error!");
            }
          });
        })
        .catch((error) => {
          setColor("#980303");
          // notify("Lỗi khi gửi yêu cầu");
          console.log(error);
        });
    } else {
      setItemBook([]);
    }
  }, [currentUser.id_user, favorite]);

  console.log("itemBook", itemBook);

  return (
    <>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />
      <div className="p-2">
        <div className="w-full sm:w-1/2 md:w-3/5 bg-[#16afcafd] rounded-r-2xl p-2 mb-5">
          <b className="text-sm lg:text-lg text-white flex flex-row items-center">
            <Link to={"/"}> Sách</Link> {<FaAngleRight></FaAngleRight>}
            Yêu thích
          </b>
        </div>
        <div className="max-w-screen-xl mb-5 mx-auto">
          <div className="flex flex-wrap -mx-1 lg:-mx-1">
            {itemBook && itemBook.length > 0 ? (
              itemBook.map((item) => {
                return (
                  item.images &&
                  item.images[0] && (
                    <Card
                      key={item.id}
                      id={item.id}
                      genres={item.cover_type}
                      url={item.images[0].url}
                      name={item.name}
                      price={item.price}
                      original_price={item.original_price}
                      rate={item.rate}
                      onClick={() => handleRemoveFromFavorite(item.id)}
                    />
                  )
                );
              })
            ) : (
              <div className="w-full flex flex-col justify-center items-center h-40 p-5">
                <img
                  width={150}
                  src={require("~/assets/images/empty-product.jpg")}
                  alt=""
                />
                <p className="text-[#dd612f] font-bold text-base lg:text-xl">
                  Oop..... bạn chưa yêu thích sản phẩm nào!
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
