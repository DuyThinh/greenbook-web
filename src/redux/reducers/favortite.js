import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import Cookies from "js-cookie";
const favoriteSlice = createSlice({
  name: "favorite",
  initialState: {
    status: "idle",
    data: Cookies.get("FAVORITE") ? JSON.parse(Cookies.get("FAVORITE")) : [],
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addToFavorite.fulfilled, (state, action) => {
        if (!state.data.some((e) => e === action.payload)) {
          state.data = [...state.data, action.payload];
          console.log(" state.data", state.data);
          state.data && Cookies.set("FAVORITE", JSON.stringify(state.data));
          state.status = "idle";
        }
      })
      .addCase(removeFromFavorite.fulfilled, (state, action) => {
        state.data = state.data.filter((e) => e !== action.payload);
        state.data && Cookies.set("FAVORITE", JSON.stringify(state.data));
        state.status = "idle";
      })
      .addCase(clearFavorite.fulfilled, (state, action) => {
        state.data = [];
        state.data && Cookies.remove("FAVORITE");
        state.status = "idle";
      });
  },
});

export const addToFavorite = createAsyncThunk(
  "favorite/add",
  async ({ id }) => {
    console.log(id);
    try {
      return id;
    } catch (error) {
      throw error;
    }
  }
);

export const removeFromFavorite = createAsyncThunk(
  "favorite/remove",
  async ({ id }) => {
    console.log(id);
    try {
      return id;
    } catch (error) {
      throw error;
    }
  }
);
export const clearFavorite = createAsyncThunk(
  "favorite/clear",
  async (_, { rejectWithValue }) => {
    return null;
  }
);

export default favoriteSlice;
