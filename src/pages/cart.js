import React, { useCallback, useEffect, useState } from "react";
import {
  currentCartSelector,
  totalSelector,
  currentUserSelector,
  ordersSelector,
  justCreateOderSelector,
} from "../redux/selectors";
import { useDispatch, useSelector } from "react-redux";
import {
  clear,
  clearCart,
  getCart,
  updateCart,
  updateUser,
} from "../redux/reducers/auth";
import { unwrapResult } from "@reduxjs/toolkit";
import { Link, useNavigate } from "react-router-dom";
import Tippy from "@tippyjs/react";
import Button from "~/components/button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import RiseLoader from "react-spinners/RiseLoader";
import greenBookAPI from "~/api/greenBookAPI";
import {
  FaAt,
  FaExclamation,
  FaMapMarkerAlt,
  FaPhone,
  FaUserCircle,
} from "react-icons/fa";
import { createOrders, getOrders } from "~/redux/reducers/orders";
import ReactTimeAgo from "react-time-ago";
import { parseISO } from "date-fns";

const UseProductItem = (props) => {
  const [quantity, setQuantity] = useState(props.quantity);

  const handleRemoveClick = (productId, quantity) => {
    props.onUpdateProduct(productId, quantity);
  };
  const handleCheckedClick = (productId, checked) => {
    props.onChecked(checked, productId);
  };
  const increaseQuantity = (productId) => {
    setQuantity((prevQuantity) => {
      const newQuantity = prevQuantity + 1;
      props.onUpdateProduct(productId, newQuantity);
      return newQuantity;
    });
  };

  const decreaseQuantity = (productId) => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => {
        const newQuantity = prevQuantity - 1;
        props.onUpdateProduct(productId, newQuantity);
        return newQuantity;
      });
    }
  };
  return (
    <tr
      className={`${
        props.index % 2 ? "bg-[#abf7df43]" : ""
      } border-b-[1.5px] border-[#05a6dc69]`}
    >
      <td>
        <input
          id="default-checkbox"
          type="checkbox"
          value={props.id}
          defaultChecked
          onChange={(e) => {
            handleCheckedClick(e.target.value, e.target.checked);
          }}
          className=" bg-[#16a0ca] border-[#16a0ca] rounded color-[#16a0ca]"
        />
      </td>
      {props.images && (
        <td className="text-center p-2 flex justify-center items-center">
          <img width={70} src={props.images} alt={props.name} />
        </td>
      )}
      <td className="text-left p-2 w-[30%] sm:w-[50%]">
        <Link to={`/detailBook/${props.name}`}>
          <span className="text-xs lg:text-sm xl:text-base overview-one">
            {props.name}
          </span>
        </Link>
      </td>
      <td className="text-left p-2">
        <span className="text-xs lg:text-sm xl:text-base">
          {props.price.toLocaleString()} VND
        </span>
      </td>
      <td className="text-center p-2 text-xs sm:text-base">
        <p className="flex flex-row justify-between items-center">
          <button onClick={() => decreaseQuantity(props.id)}>-</button>|{" "}
          {quantity} |
          <button onClick={() => increaseQuantity(props.id)}>+</button>
        </p>
      </td>

      {props.images && (
        <td className="text-center p-2">
          <button onClick={() => handleRemoveClick(props.id, 0)}>
            <Tippy content="Xoá" placement="right">
              <b className="text-red-400"> X</b>
            </Tippy>
          </button>
        </td>
      )}
    </tr>
  );
};
export default function Cart(props) {
  const cart = useSelector(currentCartSelector);
  const order = useSelector(ordersSelector);
  const justCreated = useSelector(justCreateOderSelector);
  const quantity = useSelector(totalSelector);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const currentUser = useSelector(currentUserSelector);
  const [showCheckout, setShowCheckout] = useState(false);
  const [color, setColor] = useState("#048957");
  const [loading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);

  const [paid, setPaid] = useState([]);
  const [orderPaid, setOrderPaid] = useState([]);

  const [itemBook, setItemBook] = useState([]);
  const [selectedBooks, setSelectedBooks] = useState([]);
  const [selectedBookIds, setSelectedBookIds] = useState([]);
  const [total, setTotal] = useState(0);
  const [couponInput, setCouponInput] = useState("");
  const [coupon, setCoupon] = useState({});
  const [couponErr, setCouponErr] = useState(false);
  const [shipFee, setShipFee] = useState(0);
  const [weight, setWeight] = useState(0);
  const [quantityFinal, setQuantityFinal] = useState(quantity);

  // User
  const [firstName, setFirstName] = useState(currentUser.firstName || "");
  const [lastName, setLastName] = useState(currentUser.lastName || "");
  const [shipAddress, setShipAddress] = useState(currentUser.shipAddress);
  const [phone, setPhone] = useState(currentUser.mobile || "");

  const [isDisabled, setIsDisabled] = useState(true);

  window.scrollTo(0, 0);

  const handleCancel = () => {
    setIsDisabled(true);
    setFirstName(currentUser?.firstName ?? "");
    setLastName(currentUser?.lastName ?? "");
    setPhone(currentUser?.mobile ?? "");
    setShipAddress(currentUser?.shipAddress ?? "");
  };

  const handleEditProfile = () => {
    setIsDisabled(true);
    if (firstName.length > 2 && lastName.length > 2) {
      const user = {
        avatar: currentUser.avatar,
        first_name: firstName,
        last_name: lastName,
        date_of_birth: currentUser.dateOfBirth,
        mobile: phone,
        ship_address: shipAddress ?? "",
        default_address: currentUser.defaultAddress ?? "",
      };
      dispatch(updateUser(user))
        .then({ unwrapResult })
        .then(notify("Cập nhật thành công!"))
        .catch((err) => {
          setColor("#980303");
          notify("Vui lòng thử lại sau!");
          console.log(err);
        });
    } else {
      setColor("#980303");
      notify("Vui lòng thử lại sau!");
      console.log("error!");
    }
  };

  useEffect(() => {
    setFirstName(currentUser?.firstName ?? "");
    setLastName(currentUser?.lastName ?? "");
    setPhone(currentUser?.mobile ?? "");
    setShipAddress(currentUser?.shipAddress ?? "");
  }, [currentUser]);

  useEffect(() => {
    order &&
      paid &&
      setOrderPaid(order.filter((e) => !paid.includes(e.order.id)));
  }, [order, paid]);

  useEffect(() => {
    try {
      dispatch(getOrders())
        .then(unwrapResult)
        .catch((err) => {
          console.log(err.message);
        });
    } catch (error) {
      console.log(error);
    }
  }, [cart]);

  // For preload spinner
  const override = {
    display: "flex",
    margin: "0 auto",
  };

  // For alert
  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const handleCoupon = async () => {
    if (couponInput) {
      try {
        let response = null;
        response = await greenBookAPI.getCoupon(couponInput.trim());
        if (response.data.code === 200) {
          if (response.data.data) {
            setCoupon(response.data.data);
          } else {
            setColor("#980303");
            setCouponErr(true);
            notify(response.data.message);
          }
        } else {
          setColor("#980303");
          notify("Có lỗi xảy ra, vui lòng thử lại sau!");
          console.log(response.data.message);
        }
      } catch (error) {
        console.log(error.message);
      }
    } else {
      setColor("#980303");
      setCouponErr(true);
      notify("Vui lòng kiểm tra và thử lại!");
    }
  };

  const handleShowPayment = async () => {
    setShowCheckout(!showCheckout);
    try {
      let response = null;
      response = await greenBookAPI.calculateShippingFee(weight);
      setShipFee(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleClearCart = () => {
    dispatch(clearCart())
      .then(unwrapResult)
      .then(setColor("#048957"))
      .then(() => {
        setSelectedBooks([]);
        setItemBook();
        setTotal(0);
        notify("Đã làm sạch giỏ hàng!");
      })
      .catch((err) => {
        setColor("#980303");
        notify("Đã xảy ra lỗi, vui lòng thử lại!");
        console.log(err.message);
      });
  };

  const handleCheckout = async () => {
    try {
      await handleCreateOrders();
      setColor("#048957");
      notify("Đã tạo");
      await handleGetCart();
      await handleGetOrders();
    } catch (error) {
      setColor("#980303");
      notify("Đã xảy ra lỗi, vui lòng thử lại!");
      console.log(error.message);
    }
  };

  const handleCreateOrders = async () => {
    return new Promise((resolve, reject) => {
      dispatch(
        createOrders({ coupon: coupon?.coupon ?? "", id: selectedBookIds })
      )
        .then(setShowCheckout(false))
        .then(unwrapResult)
        .then(resolve)
        .catch(reject);
    });
  };

  const handleGetCart = async () => {
    return new Promise((resolve, reject) => {
      dispatch(getCart()).then(unwrapResult).then(resolve).catch(reject);
    });
  };

  const handleGetOrders = async () => {
    return new Promise((resolve, reject) => {
      try {
        dispatch(getOrders()).then(unwrapResult).then(resolve).catch(reject);
        setRedirect(true);
      } catch (error) {
        console.log(error);
      }
    });
  };

  const handleUpdateCart = (cartItemId, quantity) => {
    dispatch(
      updateCart({
        cartItemId,
        quantity,
      })
    )
      .then(unwrapResult)
      .then(setColor("#048957"))
      .then(() => {
        notify("Đã cập nhật!");
      })
      .catch((err) => {
        setColor("#980303");
        notify("Đã xảy ra lỗi, vui lòng thử lại!");
        console.log(err.message);
      });
  };

  const handleChecked = (checked, productId) => {
    if (
      checked &&
      !selectedBooks.some((book) => book.cartItemId === productId)
    ) {
      setSelectedBooks([
        ...selectedBooks,
        ...itemBook.filter((book) => book.cartItemId === productId),
      ]);
    } else {
      const updatedSelectedBooks = selectedBooks.filter(
        (book) => book.cartItemId !== productId
      );
      setSelectedBooks(updatedSelectedBooks);
    }
  };

  useEffect(() => {
    if (redirect) {
      if (order && order?.length > 0) {
        navigate(`/payment/${justCreated}`);
      }
    }
  }, [order]);

  useEffect(() => {
    if (selectedBooks) {
      setSelectedBookIds(selectedBooks.map((e) => e.cartItemId));
      let cost = selectedBooks.reduce((acc, selectedBook) => {
        return acc + selectedBook.price * selectedBook.quantity;
      }, 0);
      let weight = selectedBooks.reduce((acc, selectedBook) => {
        return acc + selectedBook.weight;
      }, 0);
      let quantity = selectedBooks.reduce((acc, selectedBook) => {
        return acc + selectedBook.quantity;
      }, 0);
      setWeight(weight);
      setTotal(cost);
      setQuantityFinal(quantity);
    }
  }, [selectedBooks]);

  useEffect(() => {
    // Get status order
    let getOrder = [];
    if (order.length > 0) {
      order.map((id) => {
        return (getOrder = [...getOrder, id.order.id]);
      });
    }
    const requests = getOrder.map((orderId) =>
      greenBookAPI.getPaymentStatus(orderId)
    );
    // send all request
    Promise.all(requests)
      .then((responses) => {
        let getOrderRes = [];
        responses.forEach((response) => {
          if (parseInt(response.status) === 200) {
            if (response.data.data && response.data.data) {
              getOrderRes = [...getOrderRes, response.data.data.order_idOrder];
              setPaid(getOrderRes);
            }
          } else {
            setColor("#980303");
            notify("Đã xảy ra lỗi, vui lòng try again!");
            console.log("error!");
          }
        });
      })
      .catch((error) => {
        setColor("#980303");
        notify("Lỗi khi gửi yêu cầu");
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [order]);

  useEffect(() => {
    // Get Detail book
    let getBook = [];
    if (cart.length > 0) {
      cart.map((id) => {
        return (getBook = [...getBook, id.book_idBook]);
      });
    }
    const requests = getBook.map((bookId) => greenBookAPI.getBook(bookId, ""));
    Promise.all(requests)
      .then((responses) => {
        let getBookRes = [];
        responses.forEach((response) => {
          if (response.status === 200) {
            setLoading(false);
            getBookRes = [...getBookRes, response.data.data];
            const products = getBookRes.map((item, index) => {
              return {
                id: item.id,
                price: item.price,
                name: item.name,
                weight: parseInt(item.weight),
                img: item.images[0].url,
                quantity: cart[index].quantity,
                cartItemId: cart[index].id,
              };
            });
            let cost = products.reduce((acc, product) => {
              return acc + product.price * product.quantity;
            }, 0);
            let weight = products.reduce((acc, product) => {
              return acc + product.weight;
            }, 0);
            setWeight(weight);
            setTotal(cost);
            setSelectedBooks(
              selectedBookIds?.length > 0
                ? products.filter((e) => selectedBookIds.includes(e.cartItemId))
                : products
            );
            setItemBook(products);
          } else {
            setColor("#980303");
            notify("Đã xảy ra lỗi, vui lòng try again!");
            console.log("error!");
          }
        });
      })
      .catch((error) => {
        setColor("#980303");
        notify("Lỗi khi gửi yêu cầu");
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cart]);

  return loading ? (
    <div className="grid place-items-center h-[77vh]">
      <RiseLoader
        color="#55ddef"
        loading={loading}
        cssOverride={override}
        className="relative m-auto z-50 flex items-center justify-center flex-row"
        size={50}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
      <div className="fixed inset-0 z-40 bg-[#effcfb]"></div>
    </div>
  ) : (
    <div>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />
      <div className=" p-2">
        <div className="w-full sm:w-1/2 md:w-3/5 bg-[#21bcd8fd] rounded-r-2xl p-2 mb-5">
          <b className="text-sm lg:text-lg text-white flex flex-row items-center">
            Giỏ hàng
          </b>
        </div>
        {orderPaid.length > 0 && (
          <div className="max-w-8xl px-1 md:px-5">
            <div className="flex items-center">
              <FaExclamation className="text-[#88d0fa]"></FaExclamation> Bạn
              đang có {orderPaid.length} đơn hàng chưa thanh toán.
            </div>
            <div className="grid grid-cols-3 md:grid-cols-5 gap-4">
              {orderPaid.map((e) => (
                <div
                  key={e.order.id}
                  className="rounded border-2 border-[#61ccfd9d] p-2"
                >
                  <Link to={`/payment/${e.order.id}`}>
                    <div className="overview text-sm">
                      <Tippy content={e.orderItems[0]?.book?.name}>
                        <b className="overview-one">
                          {e.orderItems[0]?.book?.name}
                        </b>
                      </Tippy>
                      <p className="text-[#61ccfdec] text-xs overview-one">
                        {e.order.total_price.toLocaleString()}đ
                      </p>
                    </div>
                  </Link>
                  <div className="flex justify-between text-xs">
                    <span className="overview-one">
                      Đ.chỉ: {e.order.address}
                    </span>{" "}
                    <span className="min-w-fit">
                      <ReactTimeAgo
                        date={parseISO(e.orderItems[0]?.created_at ?? "")}
                        locale="vi-VN"
                      />
                    </span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
        <div className="max-w-8xl mx-auto flex lg:flex-row flex-col p-1 md:p-5 rounded-lg gap-5">
          <div className="relative lg:w-4/6 w-full p-1 lg:p-5 min-h-[30vh]  bg-[#F6FFFF] rounded-2xl shadow-custom shadow-[#c5c3a3]">
            <div className="max-h-[70vh] overflow-auto">
              <p className="text-right text-xs">{quantity} sản phẩm</p>
              <table className="w-full">
                <thead>
                  <tr>
                    <th className="px-3 py-2"></th>
                    <th className="px-3 py-2"></th>
                    <th className="px-3 py-2"></th>
                    <th className="px-3 py-2"></th>
                    <th className="px-3 py-2"></th>
                    <th className="px-3 py-2"></th>
                  </tr>
                </thead>
                <tbody>
                  {cart.length > 0 ? (
                    itemBook.length > 0 &&
                    itemBook.map((item) => (
                      <UseProductItem
                        quantity={item.quantity}
                        key={item.id}
                        id={item.cartItemId}
                        name={item.name}
                        price={item.price}
                        images={item.img}
                        onUpdateProduct={handleUpdateCart}
                        onChecked={handleChecked}
                      ></UseProductItem>
                    ))
                  ) : (
                    <tr className="w-full flex flex-col justify-center items-center h-40">
                      <td>
                        <>
                          <div className="w-full flex flex-col justify-center items-center h-40 p-5">
                            <img
                              width={100}
                              src={require("~/assets/images/empty-product.jpg")}
                              alt="Empty cart"
                            />
                            <p className="text-[#dd612f] font-bold text-base lg:text-xl">
                              Oop..... Giỏ hàng chưa có sản phẩm nào!
                            </p>
                            <span>
                              <Link to={"/"}>
                                <u>Mua sắm</u>
                              </Link>
                            </span>
                          </div>
                        </>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
            <p className="flex w-[90%] justify-between items-center text-xs font-bold pt-2 cursor-pointer absolute bottom-1">
              <Link to="/"> {"<<"} tiếp tục mua sắm</Link>
              {cart.length !== 0 && (
                <span onClick={handleClearCart}>
                  <u>Loại bỏ tất cả</u>
                </span>
              )}
            </p>
          </div>
          <div className="lg:w-2/6 w-full bg-[#F6FFFF] rounded-2xl p-1 lg:p-5 shadow-custom shadow-[#c5c3a3] min-h-[50vh]">
            <p className="font-bold text-xl pb-5">Tổng đơn hàng</p>
            <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
              <span>Số lượng sản phẩm:</span>
              <span>
                {quantityFinal ? quantityFinal?.toLocaleString() : "0"}
              </span>
            </p>
            <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
              <span>Giá:</span>
              <span>
                {total ? total?.toLocaleString() : "0"}
                VND
              </span>
            </p>
            <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
              <span>Giá đã bao gồm VAT:</span>
              <span>
                {total ? total?.toLocaleString() : "0"}
                VND
              </span>
            </p>
            {coupon?.id && parseInt(total) > parseInt(coupon.min_price) ? (
              <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                <span>Áp dụng mã giảm giá:</span>
                <span>
                  <strike className="text-sm font-bold text-[#5c5b5b]">
                    {total ? total.toLocaleString() : "0"}VND
                  </strike>
                  {"  "}
                  {total
                    ? coupon.is_percent
                      ? (
                          total -
                          (total * coupon.amount < coupon.limit_price
                            ? total * coupon.amount
                            : coupon.limit_price)
                        ).toLocaleString()
                      : (
                          total -
                          (coupon.amount < coupon.limit_price
                            ? coupon.amount
                            : coupon.limit_price)
                        ).toLocaleString()
                    : "0"}
                  VND
                </span>
              </p>
            ) : (
              coupon?.id && (
                <p className="text-sm text-[#972323]">
                  Mã giảm giá chỉ áp dụng cho đơn hàng từ{" "}
                  {coupon.min_price.toLocaleString()}
                </p>
              )
            )}

            <div className="py-2 font-semibold text-md flex flex-row justify-between items-center">
              <div>
                <input
                  type="text"
                  id="discount"
                  name="paymentMethod"
                  placeholder="Mã giảm giá"
                  value={couponInput}
                  onChange={(e) => {
                    setCouponInput(e.target.value);
                    setCouponErr(false);
                    setCoupon({});
                  }}
                  className={`px-3 py-2 rounded-lg text-sm font-normal border-2 ${
                    couponErr ? "border-[#dc052969]" : "border-[#05a6dc69]"
                  } `}
                />
                <label
                  htmlFor="discount"
                  className={`pl-3 text-base text-blue-400 ${
                    couponInput && "cursor-pointer"
                  }`}
                  onClick={() => couponInput && handleCoupon()}
                >
                  Áp dụng
                </label>
              </div>
            </div>
            {cart.length !== 0 && selectedBooks.length !== 0 && (
              <div className="text-center pt-9">
                <Button
                  name={"pay"}
                  id={"pay"}
                  type={"button"}
                  onClick={() => {
                    handleShowPayment();
                  }}
                  className="bg-[#21bcd8fd] text-white text-md md:text-lg font-bold p-2"
                >
                  <Tippy content="Thanh toán" placement="bottom">
                    <p>Xác nhận</p>
                  </Tippy>
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
      {cart && showCheckout && (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50">
            <div className=" w-[100vw] relative my-6">
              {/*content*/}
              <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-2/3 lg:w-1/2 bg-[#f7fdfc] py-12 px-5">
                {/*header*/}
                <div className="flex flex-row items-center w-full lg:w-4/5">
                  <p className="font-bold text-lg">Xác nhận đơn hàng</p>
                  <button
                    className="text-xl absolute right-0 top-0 text-red-500 font-bold uppercase px-2 lg:px-6 py-2"
                    type="button"
                    onClick={handleShowPayment}
                  >
                    X
                  </button>
                </div>
                {/*body*/}
                <div className="relative md:p-6 flex-auto overflow-auto max-h-[70vh]">
                  <div className="py-1 lg:py-5 max-h-96 overflow-auto">
                    <ol>
                      {cart &&
                        cart.length > 0 &&
                        selectedBooks.map((item, index) => (
                          <li
                            key={index}
                            className="py-1 border-b-[1.5px] border-[#05a6dc69]"
                          >
                            <div className="flex flex-row gap-2">
                              <img width={50} src={item.img} alt={item.name} />
                              <div className="flex flex-col justify-between w-full">
                                <p className="overview">{item.name}</p>
                                <div className="flex flex-row justify-between items-end">
                                  <p className="flex flex-row justify-between items-center text-xs ">
                                    Số lượng: {item.quantity}
                                  </p>
                                  <span className="text-[#1e7c88] text-sm">
                                    Giá: {item.price.toLocaleString()} VND
                                  </span>
                                </div>
                              </div>
                            </div>
                          </li>
                        ))}
                    </ol>
                  </div>
                  <div>
                    <div className="flex flex-col">
                      <ul>
                        <li className="py-2 flex ">
                          <div className="font-normal text-base flex items-center justify-between w-full">
                            <div>
                              Giá dự tính: {"  "}
                              <span>
                                {total?.toLocaleString() ?? (
                                  <span className="text-red-500">0</span>
                                )}{" "}
                                VND
                              </span>
                            </div>
                            <div>
                              Phí vận chuyển: {"  "}
                              <span>
                                {shipFee?.toLocaleString() ?? (
                                  <span className="text-red-500">0</span>
                                )}{" "}
                                VND
                              </span>
                            </div>
                          </div>
                        </li>
                        {coupon?.id &&
                        parseInt(total) > parseInt(coupon.min_price) ? (
                          <>
                            <li className="py-2 flex ">
                              <p className="text-md flex flex-row justify-between items-center">
                                <span>Áp dụng mã giảm giá: </span>
                                <span>
                                  {total
                                    ? coupon.is_percent
                                      ? (
                                          total -
                                          (total * coupon.amount <
                                          coupon.limit_price
                                            ? total * coupon.amount
                                            : coupon.limit_price)
                                        ).toLocaleString()
                                      : (
                                          total -
                                          (coupon.amount < coupon.limit_price
                                            ? coupon.amount
                                            : coupon.limit_price)
                                        ).toLocaleString()
                                    : "0"}
                                  VND
                                </span>
                              </p>
                            </li>
                            <li className="py-2 flex ">
                              <p className="font-semibold text-base">
                                Tổng: {"  "}
                                <b>
                                  {coupon.is_percent
                                    ? (
                                        total -
                                        (total * coupon.amount <
                                        coupon.limit_price
                                          ? total * coupon.amount
                                          : coupon.limit_price) +
                                        shipFee
                                      ).toLocaleString()
                                    : (
                                        total -
                                        (coupon.amount < coupon.limit_price
                                          ? coupon.amount
                                          : coupon.limit_price) +
                                        shipFee
                                      ).toLocaleString()}
                                  VND
                                </b>
                              </p>
                            </li>
                          </>
                        ) : (
                          <li className="py-2 flex ">
                            <p className="font-semibold text-base">
                              Tổng: {"  "}
                              <b>{(total + shipFee).toLocaleString()} VND</b>
                            </p>
                          </li>
                        )}
                        <li className="w-full flex gap-5 justify-end item-center">
                          {isDisabled ? (
                            <span
                              className="cursor-pointer text-sm"
                              onClick={() => setIsDisabled(false)}
                            >
                              <u>Chỉnh sửa</u>
                            </span>
                          ) : (
                            <>
                              <span
                                className="cursor-pointer text-sm"
                                onClick={() => handleEditProfile()}
                              >
                                <u className="text-[#21bcd8fd]">Xác nhận</u>
                              </span>
                              <span
                                className="cursor-pointer text-sm"
                                onClick={() => handleCancel()}
                              >
                                <u className="text-[#d82d21fd]">Huỷ</u>
                              </span>
                            </>
                          )}
                        </li>
                        <li className="py-2 flex gap-5">
                          <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-1/2">
                            <label
                              htmlFor="discount"
                              className="pr-3 text-base text-gray-900"
                            >
                              <FaUserCircle></FaUserCircle>
                            </label>
                            <input
                              type="text"
                              name="firstName"
                              id="firstName"
                              placeholder="Họ"
                              onChange={(e) => setFirstName(e.target.value)}
                              defaultValue={firstName}
                              disabled={isDisabled}
                              className="w-full"
                            />
                          </div>
                          <div className="flex items-center px-3 rounded-lg border-2 border-blue-200 w-1/2">
                            <input
                              type="text"
                              id="lastName"
                              name="lastName"
                              placeholder="Tên"
                              onChange={(e) => setLastName(e.target.value)}
                              defaultValue={lastName}
                              className="w-full"
                              disabled={isDisabled}
                            />
                          </div>
                        </li>
                        <li className="py-2 flex gap-5">
                          <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-full">
                            <label
                              htmlFor="discount"
                              className="pr-3 text-base text-gray-900"
                            >
                              <FaMapMarkerAlt></FaMapMarkerAlt>
                            </label>
                            <input
                              type="text"
                              id="address"
                              name="address"
                              placeholder="Địa chỉ nhận hàng"
                              onChange={(e) => setShipAddress(e.target.value)}
                              defaultValue={shipAddress}
                              className="w-full"
                              disabled={isDisabled}
                            />
                          </div>
                        </li>
                        <li className="py-2 flex gap-5">
                          <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-full">
                            <label
                              htmlFor="discount"
                              className="pr-3 text-base text-gray-900"
                            >
                              <FaPhone></FaPhone>
                            </label>
                            <input
                              type="number"
                              name="phone"
                              id="phone"
                              placeholder="Số điện thoại"
                              onChange={(e) => setPhone(e.target.value)}
                              defaultValue={phone}
                              className="w-full"
                              disabled={isDisabled}
                            />
                          </div>
                        </li>
                      </ul>
                    </div>

                    <div className="text-center pt-9">
                      <Button
                        type={"submit"}
                        onClick={() => handleCheckout()}
                        className="bg-[#21bcd8fd] shadow-md hover:shadow-none shadow-[#5f5e5eb5] text-white text-md md:text-lg font-extrabold p-2"
                      >
                        <p>Tạo đơn hàng</p>
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      )}
    </div>
  );
}
