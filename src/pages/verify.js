import Tippy from "@tippyjs/react";
import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import Button from "~/components/button";
import { unwrapResult } from "@reduxjs/toolkit";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import { verify } from "~/redux/reducers/auth";
import { isLoggedInSelector, isVerifiedSelector } from "~/redux/selectors";

export default function Verify(props) {
  const isVerified = useSelector(isVerifiedSelector);
  const isLoggedIn = useSelector(isLoggedInSelector);
  const dispatch = useDispatch();
  const [color, setColor] = useState("#048957");
  const [clickHandleVerify, setClickHandleVerify] = useState(false);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const code = searchParams.get("code");

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const handleVerify = () => {
    if (code.length > 0) {
      dispatch(verify(code))
        .then({ unwrapResult })
        .then(setClickHandleVerify(true))
        .catch((err) => {
          setColor("#980303");
          notify(err);
          console.log(err);
        });
    } else {
      setColor("#980303");
      notify("Vui lòng kiểm tra lại mã!");
    }
  };

  useEffect(() => {
    if (isVerified == "true" || isVerified == "Verify user succesfully") {
      setColor("#048957");
      notify("Verify thành công!");
    } else if (isVerified == false) {
    } else {
      setColor("#980303");
      notify(isVerified);
    }
  }, [isVerified, clickHandleVerify]);

  return isVerified == "true" || isVerified == "Verify user succesfully" ? (
    isLoggedIn == true ? (
      <>
        <ToastContainer
          toastStyle={{
            backgroundColor: "#b8fcf6ea",
            color: color,
            marginTop: "10vh",
          }}
        />
        <div className="w-full flex flex-col justify-center items-center h-[70vh] p-5">
          <img
            width={400}
            src={require("~/assets/images/welcome.png")}
            alt="Welcome new member!"
          />
          <p className="text-[#103629] font-bold text-sm lg:text-xl py-5 text-center">
            Xác thực thành công!
          </p>
          <Button
            name={"Home"}
            id={"Home"}
            type={"button"}
            className="bg-[#04d6d6] text-white text-md md:text-lg font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
          >
            <Link to="/">
              <p>Trang chủ</p>
            </Link>
          </Button>
        </div>
      </>
    ) : (
      <>
        <ToastContainer
          toastStyle={{
            backgroundColor: "#b8fcf6ea",
            color: color,
            marginTop: "10vh",
          }}
        />
        <div className="w-full flex flex-col justify-center items-center h-[70vh] p-5">
          <img
            width={400}
            src={require("~/assets/images/welcome.png")}
            alt="Welcome new member!"
          />
          <p className="text-[#103629] font-bold text-sm lg:text-xl py-5 text-center">
            Xác thực thành công!
          </p>
          <Button
            name={"Home"}
            id={"Home"}
            type={"button"}
            className="bg-[#04d6d6] text-white text-md md:text-lg font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
          >
            <Link to="/profile">
              <p>Đăng nhập</p>
            </Link>
          </Button>
        </div>
      </>
    )
  ) : (
    <>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />
      <div className="w-full flex flex-col justify-center items-center h-[70vh] p-5">
        <img
          width={400}
          src={require("~/assets/images/welcome.png")}
          alt="Welcome new member!"
        />
        <p className="text-[#103629] font-bold text-sm lg:text-xl py-5 text-center">
          Chào mừng thành viên mới, vui lòng nhấn Verify để xác thực tài khoản.
          của bạn!
        </p>
        <Button
          name={"Verify"}
          id={"Verify"}
          type={"button"}
          onClick={() => handleVerify()}
          className="bg-[#04d6a1] text-white text-md md:text-lg font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
        >
          <Tippy content="Verify" placement="bottom">
            <p>Verify</p>
          </Tippy>
        </Button>
      </div>
    </>
  );
}
