import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import greenBookAPI from "~/api/greenBookAPI";
const ordersSlice = createSlice({
  name: "orders",
  initialState: { status: "idle", data: [], justCreated: "" },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getOrders.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(getOrders.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "idle";
      })
      .addCase(createOrders.fulfilled, (state, action) => {
        state.justCreated = action.payload;
        state.status = "idle";
      })
      .addCase(getOrders.rejected, (state, action) => {
        state.status = "error";
      });
  },
});

export const createOrders = createAsyncThunk(
  "books/createOrders",
  async ({ coupon, id }) => {
    console.log(coupon, id);
    try {
      const res = await greenBookAPI.createOrder(coupon, id);
      console.log(res.data);
      if (res.status === 200) {
        const result = res.data;
        return result.data.id;
      }
    } catch (error) {
      throw error;
    }
  }
);

export const getOrders = createAsyncThunk("books/getOrders", async () => {
  try {
    const res = await greenBookAPI.getOrders();
    if (res.status === 200) {
      const result = res.data;
      return result.data;
    }
  } catch (error) {
    throw error;
  }
});

export const cancelOrder = createAsyncThunk(
  "books/cancelOrders",
  async ({ id }) => {
    try {
      const res = await greenBookAPI.cancelOrder(id);
      if (res.status === 200) {
        const result = res.data;
        return result.data;
      }
    } catch (error) {
      throw error;
    }
  }
);

export default ordersSlice;
