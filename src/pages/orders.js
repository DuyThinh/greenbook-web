import { unwrapResult } from "@reduxjs/toolkit";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getOrders } from "~/redux/reducers/orders";
import { currentUserSelector, ordersSelector } from "~/redux/selectors";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Tippy from "@tippyjs/react";
import ReactTimeAgo from "react-time-ago";
import { parseISO } from "date-fns";
import greenBookAPI from "~/api/greenBookAPI";
import { FaMapMarkerAlt, FaPhone, FaUserCircle } from "react-icons/fa";

const OrderItem = (props) => {
  const [openCollapse, setOpenCollapse] = useState(true);

  return (
    <>
      <div key={props.id} className="py-2 text-xs md:text-base">
        {props.address}
      </div>
      <div className="">
        <p className="text-xs md:text-base overview-one">
          {props.length} sản phẩm
        </p>
      </div>
      <div className="">
        <p className="text-[#1f396a] overview-one text-xs md:text-base">
          {props.totalPrice.toLocaleString()}đ
        </p>
      </div>
      <div className=" text-xs md:text-base">
        <ReactTimeAgo date={parseISO(props.createdAt)} locale="vi-VN" />
      </div>
      <div className="">
        <Tippy placement="bottom" content={props.deliveryStatus}>
          <div
            className={`${
              props.deliveryStatus === "Mới" ? "bg-[#c63623]" : "bg-[#23c67a]"
            } w-fit px-3 py-1 text-xs rounded-lg md:text-sm font-bold text-white text-center`}
          >
            {props.deliveryStatus}
          </div>
        </Tippy>
      </div>
      <div className="">
        <Tippy placement="bottom" content={props.status}>
          <div
            className={`${
              props.status === "Successfully" ? "bg-[#23c67a]" : "bg-[#c63623]"
            } w-fit px-2 py-1 text-xs rounded-lg md:text-sm font-bold text-white text-center`}
          >
            {props.status === "Successfully"
              ? "Thành công"
              : props.status === "Failure"
              ? "Thanh toán thất bại"
              : "Chưa thanh toán"}
          </div>
        </Tippy>
      </div>
      <div
        className=""
        onClick={() => {
          props.setOrderSelected(props.id);
          props.setOpenCollapse(props.id);
          props.setIndex(props.index);
        }}
      >
        <p className="flex justify-end items-center px-5">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className={`${
              !openCollapse ? "rotate-180" : ""
            } h-5 w-5 transition-transform`}
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
            onClick={() => setOpenCollapse(!openCollapse)}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M19 9l-7 7-7-7"
            />
          </svg>
        </p>
      </div>
    </>
  );
};

export default function Order(props) {
  const orders = useSelector(ordersSelector);
  const dispatch = useDispatch();
  const [color, setColor] = useState("#048957");
  const [openCollapse, setOpenCollapse] = useState(true);
  const [orderSelected, setOrderSelected] = useState("");
  const [index, setIndex] = useState("");
  const [statusOrder, setStatusOrder] = useState("");
  const [orderBook, setOrderBook] = useState([]);
  const currentUser = useSelector(currentUserSelector);

  const [firstName, setFirstName] = useState(currentUser.firstName || "");
  const [lastName, setLastName] = useState(currentUser.lastName || "");
  const [shipAddress, setShipAddress] = useState(currentUser.shipAddress);
  const [phone, setPhone] = useState(currentUser.mobile || "");

  // For alert
  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  useEffect(() => {
    try {
      dispatch(getOrders())
        .then(unwrapResult)
        .catch((err) => {
          console.log(err.message);
        });
    } catch (error) {
      console.log(error);
    }
  }, []);

  useEffect(() => {
    // Get Detail book
    let getBook = [];
    if (orders.length > 0) {
      orders.map((order) => {
        return (getBook = [
          ...getBook,
          ...order.orderItems.map((e) => e.book.id),
        ]);
      });
    }
    const requests = getBook.map((bookId) =>
      greenBookAPI.getBook(bookId, "", currentUser.id_User)
    );
    // send all request
    Promise.all(requests)
      .then((responses) => {
        let getBookRes = [];
        // For each response
        responses.forEach((response, index) => {
          if (response.status === 200) {
            getBookRes = [...getBookRes, response.data.data];
            const products = getBookRes.map((item, index) => {
              return {
                orderId:
                  orders && orders[index]?.order && orders[index]?.order?.id,
                status:
                  orders && orders[index]?.order && orders[index]?.order?.id,
                id: item.id,
                price: item.price,
                name: item.name,
                weight: parseInt(item.weight),
                img: item.images[0].url,
              };
            });
            setOrderBook(products);
          } else {
            setColor("#980303");
            notify("Đã xảy ra lỗi, vui lòng try again!");
            console.log("error!");
          }
        });
      })
      .catch((error) => {
        setColor("#980303");
        // notify("Lỗi khi gửi yêu cầu");
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [orders]);

  useEffect(() => {
    // Get status order
    let getOrder = [];
    if (orders.length > 0) {
      orders.map((id) => {
        return (getOrder = [...getOrder, id.order.id]);
      });
    }
    const requests = getOrder.map((orderId) =>
      greenBookAPI.getPaymentStatus(orderId)
    );
    // send all request
    Promise.all(requests)
      .then((responses) => {
        let getOrderRes = [];
        responses.forEach((response) => {
          getOrderRes = [...getOrderRes, response.data.message];
          setStatusOrder([...statusOrder, ...getOrderRes]);
        });
      })
      .catch((error) => {
        setColor("#980303");
        // notify("Lỗi khi gửi yêu cầu");
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [orders]);

  return (
    <>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />

      <div className="md:w-4/5 w-full mx-auto shadow-[#9a6f55a4] relative md:p-3 p-1 shadow-lg rounded-xl">
        <div className={`flex-auto overflow-auto`}>
          <div className="grid grid-cols-7 gap-2">
            <div className="py-2 text-xs md:text-sm font-semibold">Địa chỉ</div>
            <div className="">
              <p className="text-xs md:text-sm font-semibold overview-one">
                Số sản phẩm
              </p>
            </div>
            <div className="">
              <p className="overview-one text-xs md:text-sm font-semibold">
                Số tiền
              </p>
            </div>
            <div className=" text-xs md:text-sm font-semibold">Thời gian</div>
            <div className="text-xs md:text-sm font-semibold">
              Trạng thái giao hàng
            </div>
            <div className="text-xs md:text-sm font-semibold">
              Trạng thái thanh toán
            </div>
            <div></div>
            {orders &&
              orders.map((item, index) => (
                <OrderItem
                  key={item.order.id}
                  id={item.order.id}
                  address={item.order.address}
                  length={item.orderItems.length}
                  totalPrice={item.order.total_price}
                  createdAt={item.orderItems[0].created_at}
                  deliveryStatus={item.order.status}
                  status={statusOrder[index]}
                  index={index}
                  setOrderSelected={(id) => setOrderSelected(id)}
                  setIndex={(index) => setIndex(index)}
                  setOpenCollapse={(id) => {
                    setOpenCollapse(
                      orderSelected === id ? !openCollapse : false
                    );
                  }}
                  orderSelected={orderSelected}
                ></OrderItem>
              ))}
          </div>
          <div
            className={`flex-auto overflow-auto ${
              openCollapse === false ? "" : "hidden"
            }`}
          >
            <div className="w-full bg-[#F6FFFF] rounded-2xl p-1 lg:p-5">
              <div>
                <p className="px-5 py-2 text-center bg-[#21bcd8fd] text-white font-bold rounded">
                  THÔNG TIN ĐƠN HÀNG
                </p>
                <div className="flex flex-col">
                  <b className="items-center py-3">Thông tin chủ đơn hàng</b>
                  <ul className="py-2">
                    <li>
                      <div className="flex justify-between items-center">
                        <span>
                          Tình trạng đơn hàng:{" "}
                          <u
                            className={`${
                              orders[index]?.order?.status === "Mới"
                                ? "text-[#c63623]"
                                : "text-[#23c67a]"
                            }`}
                          >
                            <b> {orders[index]?.order?.status ?? "Mới"}</b>
                          </u>
                        </span>
                        <span>
                          Tình trạng thanh toán:{" "}
                          <u
                            className={`${
                              statusOrder[index] === "Successfully"
                                ? "text-[#23c67a]"
                                : "text-[#c63623]"
                            }`}
                          >
                            <b>
                              {statusOrder[index] === "Successfully"
                                ? "Thành công"
                                : statusOrder[index] === "Failure"
                                ? "Thanh toán thất bại"
                                : "Chưa thanh toán"}
                            </b>
                          </u>
                        </span>
                      </div>
                    </li>
                    <li className="py-2 flex gap-5">
                      <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-1/2">
                        <label
                          htmlFor="discount"
                          className="pr-3 text-base text-gray-900"
                        >
                          <FaUserCircle></FaUserCircle>
                        </label>
                        <input
                          type="text"
                          name="firstName"
                          id="firstName"
                          placeholder="Họ"
                          onChange={(e) => setFirstName(e.target.value)}
                          defaultValue={firstName}
                          disabled
                          className="w-full"
                        />
                      </div>
                      <div className="flex items-center px-3 rounded-lg border-2 border-blue-200 w-1/2">
                        <input
                          type="text"
                          id="lastName"
                          name="lastName"
                          placeholder="Tên"
                          disabled
                          onChange={(e) => setLastName(e.target.value)}
                          defaultValue={lastName}
                          className="w-full"
                        />
                      </div>
                    </li>
                    <li className="py-2 flex gap-5">
                      <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-full">
                        <label
                          htmlFor="discount"
                          className="pr-3 text-base text-gray-900"
                        >
                          <FaMapMarkerAlt></FaMapMarkerAlt>
                        </label>
                        <input
                          type="text"
                          id="address"
                          name="address"
                          placeholder="Địa chỉ nhận hàng"
                          disabled
                          onChange={(e) => setShipAddress(e.target.value)}
                          defaultValue={
                            orders[index]?.order?.address ?? shipAddress
                          }
                          className="w-full"
                        />
                      </div>
                    </li>
                    <li className="py-2 flex gap-5">
                      <div className="flex items-center py-2 px-3 rounded-lg border-2 border-blue-200 w-full">
                        <label
                          htmlFor="discount"
                          className="pr-3 text-base text-gray-900"
                        >
                          <FaPhone></FaPhone>
                        </label>
                        <input
                          type="number"
                          name="phone"
                          id="phone"
                          disabled
                          placeholder="Số điện thoại"
                          onChange={(e) => setPhone(e.target.value)}
                          defaultValue={phone}
                          className="w-full"
                        />
                      </div>
                    </li>
                    <li>
                      <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                        <span>Giá </span>
                        <span>
                          {orders[index]?.order.sub_price?.toLocaleString() ??
                            "0"}
                          đ
                        </span>
                      </p>
                    </li>
                    <li>
                      <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                        <span>Phí vận chuyển</span>
                        <span>
                          {orders[
                            index
                          ]?.order.shipping_price?.toLocaleString() ?? "0"}
                          đ
                        </span>
                      </p>
                    </li>
                    <li>
                      <p className="py-2 font-semibold text-md flex flex-row justify-between items-center">
                        <span>Tổng đơn hàng</span>
                        <b>
                          {orders[index]?.order.total_price?.toLocaleString()}đ
                        </b>
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <b className="items-center px-5 py-3">Sách đã mua</b>
            <table className="w-full">
              <thead>
                <tr className="rounded-t-sm">
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                  <th className="px-3 py-2"></th>
                </tr>
              </thead>
              <tbody>
                {orderBook &&
                  orderBook
                    .filter((e) => e.orderId === orderSelected)
                    .map((item, index) => (
                      <tr
                        className="gap-5 before:border-0 after:border-0"
                        key={item.id}
                      >
                        <td className="py-2 ">
                          <Link
                            to={`/detailBook/${encodeURIComponent(item.name)}`}
                          >
                            <img width={70} src={item.img} alt="item.name" />{" "}
                          </Link>
                        </td>
                        <td className="py-2 ">
                          <Link
                            to={`/detailBook/${encodeURIComponent(item.name)}`}
                          >
                            {item.name}
                          </Link>
                        </td>
                        <td className="2">
                          <p className="text-[#1f396a] overview-one text-xs md:text-base">
                            {item.price.toLocaleString()}đ
                          </p>
                        </td>
                      </tr>
                    ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
