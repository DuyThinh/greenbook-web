import React, { useEffect, useState } from "react";
import { FaAngleRight } from "react-icons/fa";
import { Link, useParams, useNavigate } from "react-router-dom";
import greenBookAPI from "~/api/greenBookAPI";
import Card from "./components/card";
import Button from "~/components/button";
import Tippy from "@tippyjs/react";
import { addToFavorite, removeFromFavorite } from "~/redux/reducers/favortite";
import { unwrapResult } from "@reduxjs/toolkit";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { favoriteSelector } from "~/redux/selectors";

export default function Books() {
  const [itemBook, setItemBook] = useState([]);
  const [namePublisher, setNamePublisher] = useState("");
  const [color, setColor] = useState("#048957");
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const favorite = useSelector(favoriteSelector);
  const limit = 10;
  const navigate = useNavigate();
  const param = useParams();

  const loadMore = async () => {
    setPage(page + 1);
  };

  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  const handleAddToFavorite = async (id) => {
    try {
      await dispatch(addToFavorite({ id })).then(unwrapResult);
      notify("Đã thêm vào yêu thích");
    } catch (error) {
      console.log(error);
    }
  };

  const handleRemoveFromFavorite = (id) => {
    try {
      dispatch(
        removeFromFavorite({
          id: id,
        })
      )
        .then(unwrapResult)
        .then(notify("Đã loại bỏ khỏi yêu thích"))
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {}
  };

  useEffect(() => {
    const handleLoadMore = async () => {
      let response = null;
      param.type === "genre"
        ? (response = await greenBookAPI.getGenre("", param.param, limit, page))
        : (response = await greenBookAPI.getPublisher(
            param.param,
            limit,
            page
          ));
      response &&
        setItemBook([
          ...itemBook,
          ...response?.data?.data?.books.filter(
            (item) => item?.is_deleted !== true
          ),
        ]);
    };

    handleLoadMore();
  }, [page]);

  // Get book, genres, genres's book
  useEffect(() => {
    const getBook = async () => {
      try {
        let response = null;
        param.type === "genre"
          ? (response = await greenBookAPI.getGenre(
              "",
              param.param,
              limit,
              page
            ))
          : (response = await greenBookAPI.getPublisher(
              param.param,
              limit,
              page
            ));
        response && setNamePublisher(response?.data?.data?.publisher);

        setItemBook(
          response.data.data.books.filter((item) => item.is_deleted !== true)
        );
      } catch (error) {
        console.log(error);
      }
    };
    getBook();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param.param, param.type]);

  return (
    <>
      <ToastContainer
        toastStyle={{
          backgroundColor: "#b8fcf6ea",
          color: color,
          marginTop: "10vh",
        }}
      />
      <div className="p-2">
        <div className="w-full sm:w-1/2 md:w-3/5 bg-[#16afcafd] rounded-r-2xl p-2 mb-5">
          <b className="text-sm lg:text-lg text-white flex flex-row items-center">
            <Link to={"/"}> Sách</Link> {<FaAngleRight></FaAngleRight>}
            {param.type === "genre"
              ? "Thể loại"
              : param.type === "publisher"
              ? "Nhà xuất bản"
              : navigate("/")}
            {<FaAngleRight></FaAngleRight>}
            {param.type === "genre"
              ? param.param
              : param.type === "publisher" &&
                namePublisher &&
                namePublisher?.first_name + " " + namePublisher?.last_name}
          </b>
        </div>
        <div className="max-w-screen-xl mb-5 mx-auto">
          <div className="flex flex-wrap -mx-1 lg:-mx-1">
            {itemBook ? (
              itemBook.map((item) => {
                return (
                  item.images &&
                  item.images[0] && (
                    <Card
                      key={item.id}
                      id={item.id}
                      genres={item.cover_type}
                      url={item.images[0].url}
                      name={item.name}
                      price={item.price}
                      original_price={item.original_price}
                      rate={item.rate}
                      onClick={() => {
                        favorite.some((e) => e === item.id)
                          ? handleRemoveFromFavorite(item.id)
                          : handleAddToFavorite(item.id);
                      }}
                    />
                  )
                );
              })
            ) : (
              <div className="w-full flex flex-col justify-center items-center h-40 p-5">
                <img
                  width={150}
                  src={require("~/assets/images/empty-product.jpg")}
                  alt=""
                />
                <p className="text-[#dd612f] font-bold text-base lg:text-xl">
                  Oop..... Xin lỗi! Hiện tại chưa tìm thấy bất kỳ sản phẩm nào!
                </p>
              </div>
            )}
          </div>
          {itemBook && (
            <Tippy placement="bottom" content="Xem thêm">
              <div className="flex w-full justify-center items-end">
                <Button
                  className="bg-[#9cecfab9] text-[#1f878a]"
                  title=""
                  type={"button"}
                  onClick={loadMore}
                >
                  <b>
                    {"<<"} Xem thêm {">>"}
                  </b>
                </Button>
              </div>
            </Tippy>
          )}
        </div>
      </div>
    </>
  );
}
