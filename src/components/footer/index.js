import React from "react";
import "./footer.scss";

function Footer() {
  return (
    <footer
      aria-label="Site Footer"
      className="text-white parabolic-shape-footer"
    >
      <div className="max-w-screen-xl px-4 pt-10 pb-3 mx-auto sm:px-6 lg:px-8 lg:pt-12">
        <div className="text-center">
          <h2 className="text-3xl font-extrabold sm:text-5xl">
            GreenBook website
          </h2>

          <p className="max-w-sm mx-auto mt-4 ">
            Một sản phẩm đến từ @Nguyen Duy Thinh ❤
          </p>
        </div>

        <div className="pt-5 mt-10 border-t border-gray-100 sm:flex sm:items-center sm:justify-between lg:mt-15">
          <nav aria-label="Footer Navigation - Support">
            <ul className="flex flex-wrap justify-center gap-4 text-xs lg:justify-end">
              <li>
                <a href="/" className="text-white transition hover:opacity-75">
                  GreenBook website
                </a>
              </li>
            </ul>
          </nav>

          <ul className="flex justify-center gap-6 mt-8 sm:mt-0 lg:justify-end"></ul>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
