import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./reducers/auth.js";
import ordersSlice from "./reducers/orders.js";
import conversationsSlice from "./reducers/conversation.js";
import commentsSlice from "./reducers/comments.js";
import favoriteSlice from "./reducers/favortite.js";
const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    orders: ordersSlice.reducer,
    conversations: conversationsSlice.reducer,
    comments: commentsSlice.reducer,
    favorite: favoriteSlice.reducer,
  },
});

export default store;
