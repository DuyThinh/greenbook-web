import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";
import {
  FaBars,
  FaHome,
  FaRegBell,
  FaSearch,
  FaUserCircle,
  FaAngleDoubleRight,
  FaDoorOpen,
  FaAddressCard,
  FaShoppingBasket,
  FaUserTag,
  FaRegListAlt,
  FaUserEdit,
  FaRegHeart,
  FaStar,
  FaTruck,
} from "react-icons/fa";
import "./header.scss";
import greenBookAPI from "~/api/greenBookAPI";
import { useDispatch, useSelector } from "react-redux";
import {
  favoriteSelector,
  isLoggedInSelector,
  ordersSelector,
  totalSelector,
} from "~/redux/selectors";
import { logout } from "~/redux/reducers/auth";
import { unwrapResult } from "@reduxjs/toolkit";
import { debounce } from "lodash";
import { clearFavorite } from "~/redux/reducers/favortite";

function Header() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isLogin = useSelector(isLoggedInSelector);
  const favorite = useSelector(favoriteSelector);
  const order = useSelector(ordersSelector);
  const total = useSelector(totalSelector);
  const [keyWord, setKeyWord] = useState("");
  const [search, setSearch] = useState([]);
  const [publishers, setPublishers] = useState([]);
  const [genres, setGenres] = useState([]);
  const [navbar, setNavbar] = useState(false);
  const [showSearch, setShowSearch] = useState(false);
  const [navbarShow, setNavbarShow] = useState(false);
  const page = 1;
  const limit = 20;

  const showNavbar = () => {
    setNavbarShow(!navbarShow);
  };
  const handleShowSearch = () => {
    setShowSearch(!showSearch);
  };
  const handleLogOutClick = () => {
    // handleClose();
    dispatch(logout())
      .then(unwrapResult)
      .then(navigate("/profile"))
      .catch((err) => console.log(err));
    dispatch(clearFavorite())
      .then(unwrapResult)
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    const handleSearchBook = async (search) => {
      try {
        const res = await greenBookAPI.searchBook(search && search);

        if (res.status === 200) {
          const result = res.data;
          setSearch(result.data);
        }
      } catch (error) {
        throw error;
      }
    };
    const debouncedSearch = debounce(handleSearchBook, 500);
    keyWord && keyWord.length > 0 && debouncedSearch(keyWord);
  }, [keyWord]);

  useEffect(() => {
    const getBook = async (limit, page) => {
      try {
        const responsePublisher = await greenBookAPI.getPublishers(limit, page);
        const responseGenres = await greenBookAPI.getGenres(limit, page);
        setPublishers(responsePublisher.data.data);
        setGenres(responseGenres.data.data);
        window.scrollTo(0, 0);
      } catch (error) {
        console.log(error);
      }
    };
    getBook(limit, page);
  }, [limit, page]);

  useEffect(() => {
    const handleScroll = () => {
      setNavbar(window.scrollY > 200);
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  return (
    <>
      {showSearch ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative my-6 w-[100vw]">
              {/*content*/}
              <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-10/12 sm:w-2/3 bg-[#fdf2e8] outline-none focus:outline-none p-5">
                {/*header*/}
                <div className="flex flex-row items-center border-blue-100 border-2 rounded-lg w-full lg:w-4/5 px-2">
                  <h3 className="text-base pr-1 lg:text-2xl font-semibold opacity-60">
                    <FaSearch></FaSearch>
                  </h3>
                  <input
                    type="text"
                    value={keyWord}
                    onChange={(e) => setKeyWord(e.target.value)}
                    placeholder="Tìm tên sách ..."
                    className="border-none w-full max-w-sm lg:px-5 py-2"
                  />
                  <button
                    className="text-xl absolute right-0 top-0 text-red-500 font-bold uppercase px-2 lg:px-6 py-2"
                    type="button"
                    onClick={() => handleShowSearch(false)}
                  >
                    X
                  </button>
                </div>
                {/*body*/}
                <div className="relative md:p-6 flex-auto overflow-auto max-h-96">
                  <div>
                    {search && search.length > 0 ? (
                      " Kết quả (20)"
                    ) : (
                      <div className="flex-col gap-5">
                        <p>Nhập từ khoá vào ô tìm kiếm bên trên</p>
                        <p className="flex justify-center mx-auto p-3">
                          <img
                            width={100}
                            src={require("~/assets/images/empty-product.jpg")}
                            alt="Empty cart"
                          />
                        </p>
                      </div>
                    )}
                  </div>
                  <ul className="flex flex-col gap-3">
                    {search &&
                      search.map((e) => (
                        <li
                          key={e.id}
                          className="py-3 px-2 rounded-md bg-[#94faf129] hover:bg-blue-100"
                          onClick={() => handleShowSearch(false)}
                        >
                          <Link
                            to={`/detailBook/${encodeURIComponent(e.name)}`}
                          >
                            <div className="flex flex-row items-center gap-3">
                              <img
                                className="rounded"
                                width={"60rem"}
                                src={
                                  e.images
                                    ? e.images[0]?.url
                                    : "https://product.hstatic.net/200000343865/product/dam-me-tham-hiem_0_1d8ce0aeaa384faa9c9e0c863b96a66d_large.jpg"
                                }
                                alt="book"
                              />
                              <div className="flex flex-col gap-1">
                                <div className="flex flex-row gap-3 items-center">
                                  {e.genres.map((e) => (
                                    <span
                                      className="text-[#287ab0] w-fit text-xs font-extralight px-2 py-1 rounded-md bg-[#8cddf6c0]"
                                      key={e.id}
                                    >
                                      {e.name}
                                    </span>
                                  ))}
                                </div>
                                <p className="overview"> {e.name}</p>
                                <div>
                                  <p className="overview">
                                    <span className="text-[#2eb0d8]">
                                      {" "}
                                      {e.price.toLocaleString()}đ
                                    </span>
                                    {parseInt(e.original_price) >
                                      parseInt(e.price) && (
                                      <strike className="text-xs text-[#6b6a6a] pl-1">
                                        {e.original_price.toLocaleString()}đ
                                      </strike>
                                    )}
                                    <span className="flex">
                                      {[...Array(5)].map((_, index) => (
                                        <FaStar
                                          key={index}
                                          className={`text-xs ${index} fa fa-star${
                                            index < e.rate
                                              ? " text-[#f0e632]"
                                              : " text-[#4d514d57] "
                                          }`}
                                        ></FaStar>
                                      ))}
                                    </span>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </Link>
                        </li>
                      ))}
                  </ul>
                </div>
                {/*footer*/}
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

      <nav
        className={`${
          navbar ? "sticked" : ""
        } z-10 flex px-5 py-4 border-t-8 border-pink-500 border-nav-top relative`}
      >
        <div className="w-3/6 sm:w-1/6 flex items-center gap-1">
          <img
            width="50px"
            src={require("~/assets/images/logo192.png")}
            alt=""
          />
          <h3 className="border-nav-top_logo xl:text-2xl text-xl font-extrabold">
            <Link to={"/"}>GreenBook</Link>
          </h3>
        </div>
        <ul className="w-1/6 lg:w-2/6"></ul>
        {/* Show for PC */}
        <div className="w-4/6 navbar-pc flex flex-row items-center justify-between">
          <ul className=" w-4/6 sm:w-5/6 flex flex-row items-center justify-around mr-10 ">
            <li className="md:text-sm xl:text-base">
              <Link to="/">
                <b>Trang chủ</b>
              </Link>
            </li>
            <li className="md:block hidden">
              {window.location.pathname === "/" ? (
                <a href="#newest" className="md:text-sm xl:text-base">
                  <b>Mới nhất</b>
                </a>
              ) : (
                <Link to="/#newest" className="md:text-sm xl:text-base">
                  <b>Mới nhất</b>
                </Link>
              )}
            </li>
            <li className="md:block hidden">
              {window.location.pathname === "/" ? (
                <a href="#bestSeller" className="md:text-sm xl:text-base">
                  <b>Bán chạy</b>
                </a>
              ) : (
                <Link to="/#bestSeller" className="md:text-sm xl:text-base">
                  <b>Bán chạy</b>
                </Link>
              )}
            </li>
            <li className="md:text-sm xl:text-base">
              <div className="dropdown inline-block relative">
                <button className="py-2 rounded inline-flex items-center">
                  <span className="mr-1">
                    <b>Nhà xuất bản</b>
                  </span>
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
                </button>
                <ul className=" w-60 bg-[#eaf8fa] dropdown-menu hidden absolute shadow-lg shadow-indigo-500/40">
                  {publishers &&
                    publishers.slice(0, 10).map((item) => (
                      <li
                        className="w-full block whitespace-no-wrap border-b border-[#7ef0e6]"
                        key={item.id}
                      >
                        <Link
                          to={`/books/publisher/${encodeURIComponent(
                            item.user_idUser
                          )}`}
                        >
                          <p className="p-2">
                            {item.first_name + " " + item.last_name}
                          </p>
                        </Link>
                      </li>
                    ))}
                </ul>
              </div>
            </li>
            <li className="md:text-sm xl:text-base">
              <div className="dropdown inline-block relative">
                <button className="py-2 rounded inline-flex items-center">
                  <span className="mr-1">
                    <b>Thể loại</b>
                  </span>
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />{" "}
                  </svg>
                </button>
                <ul className=" w-60 bg-[#eaf8fa] dropdown-menu hidden absolute shadow-lg shadow-indigo-500/40">
                  {genres &&
                    genres.slice(0, 10).map((item) => (
                      <Link to={`/books/genre/${item.name}`} key={item.id}>
                        <li
                          className="w-full block whitespace-no-wrap border-b border-[#7ef0e6]"
                          key={item.id}
                        >
                          <p className="p-2">{item.name}</p>
                        </li>
                      </Link>
                    ))}
                </ul>
              </div>
            </li>
          </ul>
          <ul className="w-1/6 sm:w-2/6 flex flex-row items-center justify-around">
            <Tippy content="Tìm kiếm">
              <li
                className="cursor-pointer"
                onClick={() => {
                  handleShowSearch();
                }}
              >
                <FaSearch />
              </li>
            </Tippy>
            <Tippy content="Yêu thích">
              <li className="relative inline-block cursor-pointer">
                <Link to={"/favorite"}>
                  <div
                    className={` ${
                      favorite && favorite.length > 0 ? "notice-bell" : ""
                    }`}
                  >
                    <FaRegHeart />
                  </div>
                </Link>
              </li>
            </Tippy>
            {isLogin ? (
              <>
                <div className="dropdown inline-block relative">
                  <button className="py-2 rounded inline-flex items-center">
                    <span className="mr-1">
                      <FaUserCircle />
                    </span>
                  </button>
                  <ul className="bg-[#eaf8fad5] dropdown-menu hidden absolute rounded p-2 w-20 translate-x-[-30px]">
                    <Tippy placement="right" content="Tôi">
                      <li className="py-2 px-2 flex justify-center whitespace-no-wrap border-b border-[#baebd7]">
                        <Link to={"/profile"}>
                          <FaAddressCard className="text-[#2bb972]" />
                        </Link>
                      </li>
                    </Tippy>
                    {order && order?.length > 0 && (
                      <Tippy placement="right" content="Đơn hàng của tôi">
                        <li className="py-2 px-2 flex justify-center whitespace-no-wrap border-b border-[#baebd7]">
                          <Link to={"/orders"}>
                            <FaTruck className="text-[#cabf2a]" />
                          </Link>
                        </li>
                      </Tippy>
                    )}
                    <Tippy placement="right" content="Đăng xuất">
                      <li
                        className="py-2 px-2 flex justify-center whitespace-no-wrap border-b border-[#baebd7]"
                        onClick={handleLogOutClick}
                      >
                        <FaDoorOpen className="text-[#b94c2b]" />
                      </li>
                    </Tippy>
                  </ul>
                </div>
                <Tippy content="Giỏ hàng">
                  <li className="relative inline-block cursor-pointer">
                    <div
                      className={` ${total && total > 0 ? "notice-bell" : ""}`}
                    >
                      <Link to={"/cart"}>
                        <FaShoppingBasket />
                      </Link>
                    </div>
                  </li>
                </Tippy>
              </>
            ) : (
              <>
                <Tippy content="Đăng nhập">
                  <Link to={"/profile"}>
                    <li className="cursor-pointer">
                      <FaUserEdit />
                    </li>
                  </Link>
                </Tippy>
              </>
            )}
          </ul>
        </div>
        {/* show for mobile */}
        <div className={`${navbarShow ? "navbar-show nav-btn" : "hidden"}`}>
          <div className=" justify-end items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 ">
            <div className="relative w-1/2 sm:w-2/5 my-6 max-w-3xl h-full ">
              {/*content*/}
              <div className="h-full border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-[#f0fbfb]">
                {/*header*/}
                <button
                  className="p-1 mr-auto border-0 float-right text-3xl"
                  onClick={showNavbar}
                >
                  <span className=" text-red-600 h-6 w-10 text-2xl">
                    <FaAngleDoubleRight></FaAngleDoubleRight>
                  </span>
                </button>
                {/*body*/}
                <div className="relative px-6 flex-auto ul-nav-show">
                  <ul className="gap-5 w-full p-2">
                    <li onClick={showNavbar}>
                      <Link to="/" className="flex items-center">
                        <>
                          <FaHome></FaHome>
                          <b>Trang chủ</b>
                        </>
                      </Link>
                    </li>
                    <li
                      onClick={() => {
                        handleShowSearch();
                        showNavbar();
                      }}
                    >
                      <FaSearch /> <b>Tìm kiếm</b>
                    </li>

                    <li onClick={showNavbar}>
                      <FaRegHeart /> <b>Yêu thích</b>
                    </li>

                    {isLogin ? (
                      <li>
                        <div className="dropdown relative flex flex-row">
                          <button className="py-2 rounded inline-flex items-center">
                            <FaUserCircle></FaUserCircle> <b>Tôi</b>
                            <svg
                              className="fill-current h-4 w-4"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                            >
                              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />{" "}
                            </svg>
                          </button>
                          <ul className="flex-auto overflow-auto max-h-60 w-60 bg-[#eaf8fa] dropdown-menu hidden absolute rounded left-0 top-full z-10">
                            <li
                              className="py-2 px-2 flex justify-center border-b border-[#ebd3ba]"
                              onClick={showNavbar}
                            >
                              <Link
                                to={`/profile`}
                                className="flex justify-center items-center"
                              >
                                <FaAddressCard className="text-[#2bb972]" />
                                <p className="px-2 text-center">Tôi</p>
                              </Link>
                            </li>
                            {order && order?.length > 0 && (
                              <li
                                className="py-2 px-2 flex justify-center border-b border-[#ebd3ba]"
                                onClick={showNavbar}
                              >
                                <Link
                                  to={"/orders"}
                                  className="flex justify-center items-center"
                                >
                                  <FaTruck className="text-[#cabf2a]" />
                                  <p className="px-2 text-center">Đơn hàng</p>
                                </Link>
                              </li>
                            )}
                            <li
                              className="py-2 px-2 flex justify-center border-b border-[#ebd3ba]"
                              onClick={showNavbar}
                            >
                              <Link
                                to={`/cart`}
                                className="flex justify-center items-center"
                              >
                                <FaShoppingBasket className="text-[#b92b80]" />
                                <p className="px-2 text-center">Giỏ hàng</p>
                              </Link>
                            </li>
                            <li
                              className="py-2 px-2 flex justify-center border-b border-[#ebd3ba]"
                              onClick={() => {
                                handleLogOutClick();
                                showNavbar();
                              }}
                            >
                              <FaDoorOpen className="text-[#b94c2b]" />{" "}
                              <p>Đăng xuất</p>
                            </li>
                          </ul>
                        </div>
                      </li>
                    ) : (
                      <li onClick={showNavbar}>
                        <Link to={"/profile"} className="flex items-center">
                          <FaUserCircle /> <b>Người dùng</b>
                        </Link>
                      </li>
                    )}

                    <li>
                      <div className="dropdown relative flex flex-row">
                        <button className="py-2 rounded inline-flex items-center">
                          <FaUserTag></FaUserTag> <b>Nhà xuất bản</b>
                          <svg
                            className="fill-current h-4 w-4"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                          >
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />{" "}
                          </svg>
                        </button>
                        <ul className="flex-auto overflow-auto max-h-60 w-60 bg-[#eaf8fa] dropdown-menu hidden absolute rounded left-0 top-full z-10">
                          {publishers &&
                            publishers.slice(0, 10).map((item) => (
                              <li
                                key={item.id}
                                className=" py-2 px-4 block whitespace-no-wrap border-b border-[#7ef0e6]"
                                onClick={() => showNavbar()}
                              >
                                <Link
                                  to={`/books/publisher/${encodeURIComponent(
                                    item.user_idUser
                                  )}`}
                                >
                                  <p className="px-2 text-center">
                                    {item.first_name + " " + item.last_name}
                                  </p>
                                </Link>
                              </li>
                            ))}
                        </ul>
                      </div>
                    </li>
                    <li>
                      <div className="dropdown relative flex flex-row">
                        <button className="py-2 rounded inline-flex items-center">
                          <FaRegListAlt></FaRegListAlt> <b>Thể loại</b>
                          <svg
                            className="fill-current h-4 w-4"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                          >
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />{" "}
                          </svg>
                        </button>
                        <ul className="flex-auto overflow-auto max-h-60 w-60 bg-[#eaf8fa] dropdown-menu hidden absolute rounded left-0 top-full z-10">
                          {genres &&
                            genres.slice(0, 10).map((item) => (
                              <li
                                key={item.id}
                                className=" py-2 px-4 block whitespace-no-wrap border-b border-[#7ef0e6]"
                                onClick={() => showNavbar()}
                              >
                                <Link
                                  to={`/books/genre/${encodeURIComponent(
                                    item.name
                                  )}`}
                                >
                                  <p className="px-2 text-center">
                                    {item.name}
                                  </p>
                                </Link>
                              </li>
                            ))}
                        </ul>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="w-2/6 lg:hidden md:hidden sm:hidden flex justify-end items-center ">
          <button className="nav-btn " onClick={showNavbar}>
            <FaBars />
          </button>
        </div>
      </nav>
    </>
  );
}

export default Header;
