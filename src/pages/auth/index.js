import { logout, updateUser } from "~/redux/reducers/auth";
import { unwrapResult } from "@reduxjs/toolkit";
import React, { useEffect, useState } from "react";
import {
  FaBirthdayCake,
  FaEye,
  FaEyeSlash,
  FaInfo,
  FaKey,
  FaMapMarkerAlt,
  FaPhoneAlt,
  FaRetweet,
  FaUserAlt,
} from "react-icons/fa";
import DatePicker from "react-datepicker";
import { parse, differenceInYears } from "date-fns";
import { isLoggedInSelector, currentUserSelector } from "~/redux/selectors";
import { useDispatch, useSelector } from "react-redux";
import Button from "~/components/button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import SignIn from "./signInComponent";
import SignUp from "./signUpComponent";
import "./auth.scss";
import "react-datepicker/dist/react-datepicker.css";
import Tippy from "@tippyjs/react";
import moment from "moment";
import greenBookAPI from "~/api/greenBookAPI";
import { clearFavorite } from "~/redux/reducers/favortite";
import { getOrders } from "~/redux/reducers/orders";

export default function Auth() {
  const [title, setTitle] = useState("Sign In");
  const isLogin = useSelector(isLoggedInSelector);
  const currentUser = useSelector(currentUserSelector);
  const [disable, setDisable] = useState(true);

  const [dateOfBirth, setDateOfBirth] = useState(
    currentUser?.dateOfBirth ?? ""
  );
  const [firstName, setFirstName] = useState(currentUser?.firstName ?? "");
  const [lastName, setLastName] = useState(currentUser?.lastName ?? "");
  const [phone, setPhone] = useState(currentUser?.mobile ?? "");
  const [avatar, setAvatar] = useState(
    currentUser?.avatar ??
      "https://static.vecteezy.com/system/resources/previews/011/321/161/non_2x/cute-shiba-inu-puppy-with-a-grin-playful-purebred-head-and-chest-pet-cute-hand-drawn-style-perfect-for-advertising-a-kennel-pet-store-or-blog-avatar-vector.jpg"
  );
  const [defaultAddress, setDefaultAddress] = useState(
    currentUser.defaultAddress
  );
  const [shipAddress, setShipAddress] = useState(currentUser.shipAddress);
  const [color, setColor] = useState("#980303");
  const [showChangePassword, setShowChangePassword] = useState(false);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const dispatch = useDispatch();
  window.scrollTo(0, 0);

  // alert
  const notify = (content) =>
    toast(`🦄 ${content}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      className: "text-sm font-bold",
      theme: "light",
    });

  useEffect(() => {
    setFirstName(currentUser?.firstName ?? "");
    setLastName(currentUser?.lastName ?? "");
    setPhone(currentUser?.mobile ?? "");
    setDefaultAddress(currentUser?.defaultAddress ?? "");
    setAvatar(
      currentUser?.avatar ??
        "https://static.vecteezy.com/system/resources/previews/011/321/161/non_2x/cute-shiba-inu-puppy-with-a-grin-playful-purebred-head-and-chest-pet-cute-hand-drawn-style-perfect-for-advertising-a-kennel-pet-store-or-blog-avatar-vector.jpg"
    );
    setShipAddress(currentUser?.shipAddress ?? "");
    setDateOfBirth(currentUser?.dateOfBirth ?? "");
  }, [currentUser]);

  const handleSignUpSuccess = () => {
    setTitle("Sign In");
  };

  const handleCancel = () => {
    setDisable(true);
    setFirstName(currentUser?.firstName ?? "");
    setLastName(currentUser?.lastName ?? "");
    setPhone(currentUser?.mobile ?? "");
    setDefaultAddress(currentUser?.defaultAddress ?? "");
    setAvatar(
      currentUser?.avatar ??
        "https://static.vecteezy.com/system/resources/previews/011/321/161/non_2x/cute-shiba-inu-puppy-with-a-grin-playful-purebred-head-and-chest-pet-cute-hand-drawn-style-perfect-for-advertising-a-kennel-pet-store-or-blog-avatar-vector.jpg"
    );
    setShipAddress(currentUser?.shipAddress ?? "");
    setDateOfBirth(currentUser?.dateOfBirth ?? "");
  };

  const handleEditProfile = () => {
    setDisable(true);
    const parsedDate = parse(dateOfBirth, "yyyy-MM-dd", new Date());
    const currentDate = new Date();
    const age = differenceInYears(currentDate, parsedDate);
    const isValidAge = age >= 1 && age <= 123;

    if (isValidAge === true) {
      if (firstName.length > 2 && lastName.length > 2) {
        const user = {
          avatar: avatar,
          first_name: firstName,
          last_name: lastName,
          date_of_birth: dateOfBirth,
          mobile: phone,
          ship_address: shipAddress ?? "",
          default_address: defaultAddress ?? "",
        };
        dispatch(updateUser(user))
          .then({ unwrapResult })
          .then(notify("Cập nhật thành công!"))
          .catch((err) => {
            setColor("#980303");
            notify("Vui lòng thử lại sau!");
            console.log(err);
          });
      } else {
        setColor("#980303");
        notify("Vui lòng thử lại sau!");
        console.log("error!");
      }
    } else {
      notify("Vui lòng nhập ngày sinh hợp lệ!");
    }
  };

  const handleLogOutClick = () => {
    // handleClose();
    dispatch(logout())
      .then(unwrapResult)
      .catch((err) => console.log(err));
    dispatch(clearFavorite())
      .then(unwrapResult)
      .catch((err) => console.log(err));
  };

  const handleChangPassword = async () => {
    if (oldPassword.length > 3 && newPassword.length > 3) {
      let response = null;
      response = await greenBookAPI.changPassword(oldPassword, newPassword);
      if (response.status === 200) {
        if (response.data.message == "Update password successfully") {
          setShowChangePassword(false);
          setOldPassword("");
          setNewPassword("");
          setColor("#980303");
          notify(response.data.message);
        } else {
          setColor("#980303");
          notify(response.data.message);
        }
      } else {
        setColor("#980303");
        notify(`Mã lỗi ${response.status}`);
      }
    } else {
      setColor("#980303");
      notify("Mật khẩu có tối thiểu 4 ký tự!");
      console.log("Mật khẩu có tối thiểu 4 ký tự!");
    }
  };

  useEffect(() => {
    if (isLogin) {
      dispatch(getOrders({}))
        .then(unwrapResult)
        .catch((err) => {
          console.log(err.message);
        });
    }
  }, [isLogin]);

  return (
    <>
      {isLogin ? (
        <>
          <ToastContainer
            toastStyle={{
              backgroundColor: "#b8fcf6ea",
              color: color,
              marginTop: "10vh",
            }}
          />
          <section className="bg-sign p-5">
            <div className="row d-flex justify-content-center align-items-center p-3 lg:p-10">
              <div className="bg-[#fcecc1c4] relative w-full md:w-2/3 mx-auto px-6 py-12 lg:p-10 rounded-xl flex flex-col sm:flex-row sm:items-start items-center  flex-wrap gap-1 sm:gap-0">
                <div className="text-white flex flex-col w-1/4 sm:pr-2">
                  <div className="mx-auto text-center">
                    <img
                      width={"150px"}
                      src={avatar}
                      alt="Generic placeholder"
                      className="img-fluid img-thumbnail mt-4 mb-2 rounded-xl"
                    />
                    <Tippy content="Get random!" placement="bottom">
                      <div>
                        <Button
                          type="button"
                          disabled={disable}
                          className="bg-[#4b4b4c] rounded-xl"
                          data-mdb-ripple-color="dark"
                          onClick={() => {
                            // Get random letter
                            const rule = () =>
                              "abcdefghijklmnopqrstuvwxyz"[
                                Math.floor(Math.random() * 26)
                              ];
                            const randomLetter = rule();
                            setAvatar(
                              `https://robohash.org/${randomLetter}?set=set5&bgset=&size=400x400`
                            );
                          }}
                        >
                          <FaRetweet></FaRetweet>
                        </Button>
                      </div>
                    </Tippy>
                  </div>
                </div>
                <div className="w-3/4">
                  <div className="flex flex-col">
                    <div className="flex flex-row items-center lg:w-3/4 w-full py-2">
                      <FaInfo className="text-2xl text-blue-500"></FaInfo>
                      <b className="text-xl">Thông tin</b>
                    </div>
                    <div className="">
                      <div className="w-full">
                        <b>Họ</b>
                        <div className="sm:text-base text-sm px-3 py-2 border-b-2 rounded-t-md border-[#695308] bg-[#fdfae163] flex flex-row items-center lg:w-2/3 w-full">
                          <FaUserAlt></FaUserAlt>
                          <input
                            className={" bg-transparent ml-5 "}
                            type={"text"}
                            name={"firstName"}
                            onChange={(e) => {
                              setFirstName(e.target.value);
                            }}
                            value={firstName}
                            disabled={disable}
                          ></input>
                        </div>
                      </div>
                      <div className="w-full">
                        <b>Tên</b>
                        <div className="sm:text-base text-sm px-3 py-2 border-b-2 rounded-t-md border-[#695308] bg-[#fdfae163] flex flex-row items-center lg:w-2/3 w-full">
                          <FaUserAlt></FaUserAlt>
                          <input
                            className={" bg-transparent ml-5 "}
                            type={"text"}
                            name={"lastName"}
                            onChange={(e) => {
                              setLastName(e.target.value);
                            }}
                            value={lastName}
                            disabled={disable}
                          ></input>
                        </div>
                      </div>
                      <div className="w-full">
                        <b>Ngày sinh</b>
                        <div className="sm:text-base text-sm px-3 py-2 border-b-2 rounded-t-md border-[#695308] bg-[#fdfae163] flex flex-row items-center lg:w-2/3 w-full">
                          <FaBirthdayCake></FaBirthdayCake>
                          <DatePicker
                            className="sm:text-base text-sm ml-5 w-full"
                            value={dateOfBirth}
                            disabled={disable}
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            maxDate={new Date()}
                            onChange={(date) => {
                              setDateOfBirth(moment(date).format("YYYY-MM-DD"));
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="py-2">
                      <b>Phone</b>
                      <div className="sm:text-base text-sm px-3 py-2 border-b-2 rounded-t-md border-[#695308] bg-[#fdfae163] flex flex-row items-center lg:w-2/3 w-full">
                        <FaPhoneAlt></FaPhoneAlt>
                        <input
                          className={" bg-transparent ml-5 "}
                          type={"number"}
                          name={"phone"}
                          onChange={(e) => {
                            setPhone(e.target.value);
                          }}
                          value={phone}
                          disabled={disable}
                        ></input>
                      </div>
                    </div>
                    <div className="py-2">
                      <b>Ship Address</b>
                      <div className="sm:text-base text-sm px-3 py-2 border-b-2 rounded-t-md border-[#695308] bg-[#fdfae163] flex flex-row items-center lg:w-2/3 w-full">
                        <FaMapMarkerAlt></FaMapMarkerAlt>
                        <input
                          className={" bg-transparent ml-5 "}
                          type={"text"}
                          name={"shipAddress"}
                          onChange={(e) => {
                            setShipAddress(e.target.value);
                          }}
                          value={shipAddress}
                          disabled={disable}
                        ></input>
                      </div>
                    </div>
                    <div className="py-2">
                      <b>Default Address</b>
                      <div className="sm:text-base text-sm px-3 py-2 border-b-2 rounded-t-md border-[#695308] bg-[#fdfae163] flex flex-row items-center lg:w-2/3 w-full">
                        <FaMapMarkerAlt></FaMapMarkerAlt>
                        <input
                          className={" bg-transparent ml-5 "}
                          type={"text"}
                          name={"address"}
                          onChange={(e) => {
                            setDefaultAddress(e.target.value);
                          }}
                          value={defaultAddress}
                          disabled={disable}
                        ></input>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flex flex-row gap-10 justify-end absolute bottom-2 right-5">
                  <Button
                    type="button"
                    className={`bg-black rounded-xl text-sm text-white`}
                    data-mdb-ripple-color="dark"
                    onClick={() => setShowChangePassword(true)}
                  >
                    Đổi mật khẩu
                  </Button>
                  {disable ? (
                    <Button
                      type="button"
                      className={`bg-black rounded-xl text-sm text-white`}
                      data-mdb-ripple-color="dark"
                      onClick={() => setDisable(false)}
                    >
                      Chỉnh sửa
                    </Button>
                  ) : (
                    <>
                      <Button
                        type="button"
                        className={`text-sm rounded-xl text-black bg-white`}
                        data-mdb-ripple-color="dark"
                        onClick={handleCancel}
                      >
                        Huỷ
                      </Button>
                      <Button
                        type="button"
                        className="bg-[#00aeff] text-sm rounded-xl text-white"
                        data-mdb-ripple-color="dark"
                        onClick={handleEditProfile}
                      >
                        Save
                      </Button>
                    </>
                  )}
                  {showChangePassword ? (
                    <>
                      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                        <div className="mx-auto border-0 rounded-lg shadow-lg relative flex flex-col w-2/3 lg:w-1/2 bg-[#fdf2e8] outline-none focus:outline-none p-5">
                          {/*header*/}
                          <div className="flex flex-col gap-1 items-center justify-center border-blue-100 border-2 rounded-lg w-full px-2 py-2">
                            <p className="py-2"> Thay đổi mật khẩu</p>
                            <div className="py-3 w-10/12 md:w-1/2">
                              <label>
                                <FaKey></FaKey>
                              </label>
                              <div className="w-full flex flex-row items-center border-b-2 border-[#695308] bg-[#fcecc18d]">
                                <input
                                  value={oldPassword}
                                  onChange={(e) =>
                                    setOldPassword(e.target.value)
                                  }
                                  type="password"
                                  autoComplete="off"
                                  name="oldPassword"
                                  id="oldPassword"
                                  className="sm:text-base text-sm px-3 py-2"
                                  placeholder="Mật khẩu cũ"
                                  required
                                />
                              </div>
                            </div>
                            <div className="py-3 w-10/12 md:w-1/2">
                              <label>
                                <FaKey></FaKey>
                              </label>
                              <div className="w-full flex flex-row items-center border-b-2 border-[#695308] bg-[#fcecc18d] relative">
                                <input
                                  value={newPassword}
                                  onChange={(e) =>
                                    setNewPassword(e.target.value)
                                  }
                                  type={`${showPassword ? "text" : "password"}`}
                                  autoComplete="off"
                                  name="newPassword"
                                  id="newPassword"
                                  className="sm:text-base text-sm px-3 py-2"
                                  placeholder="Mật khẩu mới"
                                  required
                                />
                                <span className="absolute right-0">
                                  {showPassword ? (
                                    <FaEye
                                      onClick={() => setShowPassword(false)}
                                    ></FaEye>
                                  ) : (
                                    <FaEyeSlash
                                      onClick={() => setShowPassword(true)}
                                    ></FaEyeSlash>
                                  )}
                                </span>
                              </div>
                            </div>
                          </div>
                          {/*body*/}
                          <div className="p-5 flex-auto overflow-auto mx-auto space-x-5">
                            <Button
                              name={"cancel"}
                              id={"cancel"}
                              type={"button"}
                              onClick={() => {
                                setShowChangePassword(false);
                              }}
                              className="text-black bg-white text-md md:text-base font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
                            >
                              <p className="text-sm">Huỷ</p>
                            </Button>
                            <Button
                              name={"change"}
                              id={"change"}
                              type={"button"}
                              onClick={() => {
                                handleChangPassword();
                                handleLogOutClick();
                              }}
                              className="bg-[#04d6d6] text-white text-md md:text-base font-bold p-2 shadow-[#6e6e6e9d] shadow-md hover:shadow-none"
                            >
                              <p className="text-sm">Thay đổi</p>
                            </Button>
                          </div>
                        </div>
                      </div>
                      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                  ) : null}
                </div>
              </div>
            </div>
          </section>
        </>
      ) : (
        <div className="d-flex justify-content-center flex-column align-items-center bg-sign p-10 lg:p-20 h-[100vh]">
          <div className="w-fit bg-white mx-auto rounded-lg px-3 py-2 mb-5">
            <Button
              type="button"
              className={`tabSign ${
                title === "Sign In" ? "active" : ""
              } hover:border-b-[#ffa704] hover:shadow-none border border-transparent`}
              onClick={() => {
                setTitle("Sign In");
              }}
            >
              Đăng nhập
            </Button>
            <span className="px-4">|</span>
            <Button
              type="button"
              onClick={() => {
                setTitle("Sign Up");
              }}
              className={`tabSign ${
                title === "Sign Up" ? "active" : ""
              } hover:border-b-[#ffa704] hover:shadow-none border border-transparent`}
            >
              Đăng ký
            </Button>
          </div>
          <div className="px-5">
            {title === "Sign In" ? (
              <SignIn title={title}></SignIn>
            ) : (
              <SignUp title={title} onSuccess={handleSignUpSuccess}></SignUp>
            )}
          </div>
        </div>
      )}
    </>
  );
}
