## Available Scripts

In the project directory, you can run:

### `npm install`

then run

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3001](http://localhost:3001) to view it in your browser.

## Preview

# Home page

![image](./src/assets/homePage.PNG)

# Auth page

![image](./src/assets/authPage.PNG)

# Detail page

![image](./src/assets/detailPage.png)

# Cart page

![image](./src/assets/cartPage.PNG)

# Orders page

![image](./src/assets/ordersPage.png)

# Payment page

![image](./src/assets/paymentPage.png)

## Demo

# `https://greenbook-web.vercel.app/`
