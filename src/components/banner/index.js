import React, { useEffect, useState } from "react";
import "./banner.scss";
import Button from "../button";
import { Link } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCoverflow, Pagination, Navigation } from "swiper";

import greenBookAPI from "~/api/greenBookAPI";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";

function Banner(props) {
  const [itemBook, setItemBook] = useState([]);
  const [slice, setSlice] = useState(10);

  const page = 3;
  const limit = 20;

  useEffect(() => {
    function randomDigit() {
      return Math.floor(Math.random() * 13) + 8;
    }

    var randomNumber = randomDigit();
    setSlice(randomNumber);
  }, []);

  useEffect(() => {
    const getDog = async (limit, page) => {
      try {
        const response = await greenBookAPI.getBooks(limit, page);
        setItemBook(response.data.data);
        window.scrollTo(0, 0);
      } catch (error) {
        console.log(error);
      }
    };
    getDog(limit, page);
  }, [limit, page]);

  return (
    <div className="parabolic-shape pb-5 relative">
      <span className="circle_1"></span>
      <span className="circle_2"></span>
      <span className="circle_3"></span>
      <div className="flex flex-row items-center h-full p-2">
        <div className="w-2/6 flex flex-col justify-center items-start text-banner p-2">
          <h4 className="xl:text-3xl lg:text-3xl py-2">
            Bạn muốn chìm trong kiến thức?
          </h4>
          <h2 className="xl:text-4xl lg:text-2xl text-xl font-extrabold py-2">
            Đến với GreenBook
          </h2>
          <h5 className="xl:text-3xl lg:text-2lx py-2 hidden md:block">
            Chúng tôi luôn chào đón...
          </h5>
        </div>
        <div className="w-4/6 flex flex-row items-center justify-center p-5 img-banner">
          <div className="container">
            {itemBook && itemBook.length > 0 && (
              <Swiper
                effect={"coverflow"}
                grabCursor={true}
                centeredSlides={true}
                loop={false}
                centeredSlidesBounds={true}
                slidesPerView={"auto"}
                coverflowEffect={{
                  rotate: 0,
                  stretch: 0,
                  depth: 150,
                  modifier: 1.5,
                }}
                pagination={{ el: ".swiper-pagination", clickable: true }}
                navigation={{
                  nextEl: ".swiper-button-next",
                  prevEl: ".swiper-button-prev",
                  clickable: true,
                }}
                modules={[EffectCoverflow, Pagination, Navigation]}
                className="swiper_container"
              >
                {itemBook?.length &&
                  itemBook.slice(slice - 7, slice).map((item) => {
                    return item.images.map((img, index) => {
                      return (
                        <SwiperSlide key={index}>
                          <Link to={`/detailBook/${item.name}`}>
                            <img src={img.url} alt={item.name} />
                          </Link>
                        </SwiperSlide>
                      );
                    });
                  })}

                <div className="slider-controler">
                  <div className="swiper-button-prev slider-arrow">
                    <ion-icon name="arrow-back-outline"></ion-icon>
                  </div>
                  <div className="swiper-button-next slider-arrow">
                    <ion-icon name="arrow-forward-outline"></ion-icon>
                  </div>
                  <div className="swiper-pagination"></div>
                </div>
              </Swiper>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Banner;
