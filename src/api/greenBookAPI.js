import instance from "./axios.config";
import queryString from "query-string";
import PropTypes from "prop-types";

const greenBookAPI = {
  // ~~~~~~~~~~~~~~Book~~~~~~~~~~~~~~~~//
  getBook: (id, name, idUser) => {
    const query = {
      ...(id && { bookId: id }),
      ...(name && { bookName: name }),
      ...(idUser && { userProfileId: idUser }),
    };
    const url = `books/find?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  getBooks: (limit = "10", page = "1") => {
    const query = {
      ...(limit && { rowPerPage: limit }),
      ...(page && page !== "0" && { pageNumber: page }),
    };
    const url = `books/?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  getBooksBestSeller: (limit = "10", page = "1") => {
    const query = {
      ...(limit && { rowPerPage: limit }),
      ...(page && page !== "0" && { pageNumber: page }),
    };
    const url = `books/hot-stock?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  getBooksHotStock: (limit = "10", page = "1") => {
    const query = {
      ...(limit && { rowPerPage: limit }),
      ...(page && page !== "0" && { pageNumber: page }),
    };
    const url = `books/saled?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  searchBook: (search) => {
    const query = {
      ...{ rowPerPage: "20" },
      ...{ pageNumber: "1" },
    };
    const url = `books/search?keyword=${search}&${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  // ~~~~~~~~~~~~~~Author~~~~~~~~~~~~~~~~//
  getAuthor: (id, name) => {
    const query = {
      ...(id && { authorId: id }),
      ...(name && { authorName: name }),
    };
    const url = `authors/find?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  getAuthors: (limit = "10", page = "1") => {
    const query = {
      ...(limit && { rowPerPage: limit }),
      ...(page && page !== "0" && { pageNumber: page }),
    };
    const url = `authors/?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  // ~~~~~~~~~~~~~~Genre~~~~~~~~~~~~~~~~//
  getGenre: (id, name, limit = "10", page = "1") => {
    const query = {
      ...(id && { genreId: id }),
      ...(name && { genreName: name }),
      ...{ rowPerPage: limit },
      ...{ pageNumber: page },
    };
    const url = `genres/find?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  getGenres: (limit = "10", page = "1") => {
    const query = {
      ...(limit && { rowPerPage: limit }),
      ...(page && page !== "0" && { pageNumber: page }),
    };
    const url = `genres/?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  // ~~~~~~~~~~~~~~Publisher~~~~~~~~~~~~~~~~//
  getPublisher: (id, limit = "10", page = "1") => {
    const query = {
      ...(id && { publisherId: id }),
      ...{ rowPerPage: limit },
      ...{ pageNumber: page },
    };
    const url = `publisher/find?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  getPublishers: (limit = "10", page = "1") => {
    const query = {
      ...(limit && { rowPerPage: limit }),
      ...(page && { pageNumber: page }),
    };
    const url = `publisher/?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  // ~~~~~~~~~~~~~~User~~~~~~~~~~~~~~~~//
  changPassword: (oldPassword = "", newPassword = "") => {
    const query = {
      ...(oldPassword && { oldPassword }),
      ...(newPassword && { newPassword }),
    };
    const url = `users/change_password?${queryString.stringify({
      ...query,
    })}`;
    return instance.post(url);
  },
  forgotPassword: (userEmail = "") => {
    const query = {
      ...(userEmail && { userEmail }),
    };
    const url = `user_admin/reset_password?${queryString.stringify({
      ...query,
    })}`;
    return instance.post(url);
  },

  getProfile: (id = "") => {
    const query = {
      ...(id && { userId: id }),
    };
    const url = `users/getProfile?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  // ~~~~~~~~~~~~~~Shipping~~~~~~~~~~~~~~~~//
  calculateShippingFee: (weight = "0") => {
    const query = {
      ...(weight && { weight: weight }),
    };
    const url = `shipping/caculate?${queryString.stringify({
      ...query,
    })}`;
    return instance.post(url);
  },

  // ~~~~~~~~~~~~~~Discount~~~~~~~~~~~~~~~~//
  getCoupon: (coupon = "") => {
    const query = {
      ...(coupon && { coupon: coupon }),
    };
    const url = `discount/find?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  // ~~~~~~~~~~~~~~Order~~~~~~~~~~~~~~~~//
  createOrder: (coupon = "", id = [""]) => {
    const query = {
      ...(coupon && { coupon: coupon }),
    };
    const url = `order/create?${queryString.stringify({
      ...query,
    })}`;

    return instance.post(url, id);
  },
  getOrders: () => {
    const url = `order`;
    return instance.get(url);
  },
  cancelOrder: (id = "") => {
    const query = {
      ...(id && { orderId: id }),
    };
    const url = `order?${queryString.stringify({
      ...query,
    })}`;
    return instance.delete(url);
  },

  // ~~~~~~~~~~~~~~Comments~~~~~~~~~~~~~~~~//
  addComment: (bookId = "", content = "", rate = 0, parentId) => {
    const query = {
      ...(bookId && { bookId: bookId }),
      ...(content && { content: content }),
      ...(rate && { rating: rate }),
      ...(parentId && { parentId: parentId }),
    };
    const url = `comment/add?${queryString.stringify({
      ...query,
    })}`;
    return instance.post(url);
  },

  // ~~~~~~~~~~~~~~Conversation~~~~~~~~~~~~~~~~//
  createConversation: (id = "") => {
    const query = {
      ...(id && { userIdTo: id }),
    };
    const url = `conversation/create?${queryString.stringify({
      ...query,
    })}`;
    return instance.post(url);
  },
  getConversation: (id = "") => {
    const query = {
      ...(id && { userIdTo: id }),
    };
    const url = `conversation?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
  postMessage: (id = "", content = "") => {
    const query = {
      ...(id && { conversationId: id }),
      ...(content && { content: content }),
    };
    const url = `conversation/message?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },

  // ~~~~~~~~~~~~~~Payment~~~~~~~~~~~~~~~~//
  getPaymentUrl: (id = "", amount = "0") => {
    const data = {
      order_id: id,
      amount: amount,
    };
    const url = `vnpay/payment_url`;
    return instance.post(url, data);
  },
  getPaymentStatus: (id = "") => {
    const query = {
      ...(id && { orderId: id }),
    };
    const url = `vnpay/status?${queryString.stringify({
      ...query,
    })}`;
    return instance.get(url);
  },
};

greenBookAPI.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string,
  page: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  limit: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default greenBookAPI;
