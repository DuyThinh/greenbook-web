import Home from "~/pages/home";
import Books from "~/pages/books";
import DetailBook from "~/pages/detailBook";
import Auth from "~/pages/auth";
import Cart from "~/pages/cart";
import Verify from "~/pages/verify";
import Payment from "~/pages/payment";
import Order from "~/pages/orders";
import PaymentStatus from "~/pages/paymentStatus";
import Favorite from "~/pages/favorite";

const routes = {
  home: "/",
  cart: "/cart",
  profile: "/profile",
  books: "/books/:type/:param",
  detailBook: "/detailBook/:name",
  payment: "/payment/:id",
  verify: "/verify",
  orders: "/orders",
  favorite: "/favorite",
  paymentStatus: "/payment-status",
};

export const SetRoutes = [
  { path: routes.home, components: Home },
  { path: routes.cart, components: Cart },
  { path: routes.profile, components: Auth },
  { path: routes.books, components: Books },
  { path: routes.detailBook, components: DetailBook },
  { path: routes.verify, components: Verify },
  { path: routes.payment, components: Payment },
  { path: routes.orders, components: Order },
  { path: routes.favorite, components: Favorite },
  { path: routes.paymentStatus, components: PaymentStatus },
];
